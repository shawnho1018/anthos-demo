#!/bin/bash
ACTION=$1
if [ -z "${ACTION}" ]; then
    echo "an 'ACTION' is required (deploy || delete || add-domain || enable-cloudrun-event || disable-cloudrun-event || demo-blue-green || demo-autoscale || demo-mlops)"
    exit 1
fi
function set_repo_secret() {
  SECRETNAME="gcr-secret"
  secret=$(kubectl get secret ${SECRETNAME})
  if [ -z "${secret}" ]; then
    kubectl create secret docker-registry $SECRETNAME \
      --docker-server=https://gcr.io \
      --docker-username=_json_key \
      --docker-email=${ACCOUNT_EMAIL} \
      --docker-password="$(cat ${SERVICE_KEY_PATH})"
    kubectl patch serviceaccount default \
      -p "{\"imagePullSecrets\": [{\"name\": \"$SECRETNAME\"}]}"   
  fi 
}
function add_domain_mapping() {
  if [ $# = 1 ]; then
    DOMAINNAME=$1
    gcloud domains verify ${DOMAINNAME}
    #sed -i '.original' "s/DOMAINNAME/${DOMAINNAME}/g" ./patch-domain-name.yaml
    #kubectl patch configmap config-domain --patch $(cat ./patch-domain-name.yaml)
    kubectl patch configmap config-domain --namespace knative-serving --patch \
      '{"data": {"example.com": null, "shawnk8s.com": ""}}'
  else
    echo "Only allow 1 parameter as Domain Name"
  fi
}

function enable_cloudrun_event() {
  # set eventing control-plane
  echo "initializing the eventing..."
  gcloud config set run/cluster ${CLUSTER_NAME}
  gcloud config set run/cluster_location ${ZONE}
  gcloud config set run/platform gke
  gcloud beta events init --quiet
}
function set_broker() {
  # set eventing data-plane
  echo "setting broker in the namespace event-broker..."  
  kubectl create ns ${namespace}
  gcloud beta events namespaces init ${namespace} \
  --copy-default-secret
  gcloud beta events brokers create default --namespace ${namespace}
}

function link_gcs_cloudrun() {
  echo "create gcs bucket ${gcsname}..."
  gsutil mb "gs://${gcsname}/"
  echo "create event-driven function on cloudrun ..."
  gcloud run deploy events-quickstart-receiver \
    --namespace=${namespace} \
    --service-account="default" \
    --set-env-vars service_url="http://demo.default.shawnk8s.com/predictions/captcha5-custom",bucket=${gcsname} \
    --image gcr.io/$(gcloud config get-value project)/ml-client:latest
  echo "create triggers to link gcs to event-driven function..."
  gcloud beta events triggers create trigger-storage \
    --namespace ${namespace} \
    --target-service events-quickstart-receiver \
    --type=google.cloud.storage.object.v1.finalized \
    --parameters bucket=${gcsname}
}
function disable_cloudrun_event() {
  gcloud beta events triggers delete --quiet trigger-storage --namespace ${namespace}
  gcloud run services delete --quiet events-quickstart-receiver --namespace ${namespace}
  gcloud beta events brokers delete --quiet default --namespace ${namespace}
  kubectl delete ns $namespace
  gsutil rm -r "gs://${gcsname}"
}

function bind_workload_identity() {
  gcloud iam service-accounts add-iam-policy-binding \
  --role roles/iam.workloadIdentityUser \
  --member "serviceAccount:shawn-demo-2021.svc.id.goog[${namespace}/default]" \
  anthos-install@shawn-demo-2021.iam.gserviceaccount.com
  kubectl annotate serviceaccount \
  --namespace ${namespace} \
  default \
  iam.gke.io/gcp-service-account=anthos-install@shawn-demo-2021.iam.gserviceaccount.com  
}

function demo_blue_green() {
  gcloud config set run/cluster ${CLUSTER_NAME}
  gcloud config set run/cluster_location ${ZONE}
  gcloud config set run/platform gke 
  echo -n "Welcome to Shawn Game Inc...Let's play a new maxprime game...Press any key to continue"
  read k
  # uncomment this if you want to use a public repo
  # gcloud run deploy fsr sea-anthos-demo/maxprime
  gcloud run deploy fsr --image=gcr.io/${PROJECT_ID}/maxprime:latest

  echo -n "Press any key to deploy a new game Mario"
  read k
  # uncomment this if you want to use a public repo
  # gcloud run deploy fsr pengbai/docker-supermario
  gcloud run deploy fsr --no-traffic --image=gcr.io/${PROJECT_ID}/supermario:latest
  service_revision=$(gcloud run services describe fsr | grep "Revision" | awk '{print $2}')
  serviceip=$(kubectl get svc -l app=ingressgateway -n gke-system | awk 'NR>1 {print $4}')
  echo -n "Open a browser and connect to http://${serviceip} or http://fsr.default.${DOMAINNAME}"
  echo -n "Press any key for canary deployment 25%"
  read k
  gcloud run services update-traffic fsr --to-revisions "${service_revision:0:13}=25"
  gcloud run services describe fsr

  echo -n "Press any key to update the game"
  read k
  gcloud run services update-traffic fsr --to-latest
  gcloud run services describe fsr

  echo -n "Press any key to clean up the game"
  read k
  gcloud run services delete --quiet fsr  
}

function demo_autoscale() {
  if [ ! -f cloudbuild.yaml ]; then
    echo "Copy CloudBuild.yaml for creating image"
    gsutil cp gs://demo2020-workspace-tools/cloudbuild.yaml .
  fi
  #gcloud builds submit --no-source --substitutions _DEMO_USER=$(whoami)    
  serviceip=$(kubectl get svc -l app=ingressgateway -n gke-system | awk 'NR>1 {print $4}')
  echo "Retrieve ingressgateway's ip: $serviceip"
  echo "Deploy fsr with concurrency 1 and max-instances 100"
  echo "Press any key to continue..."
  gcloud run deploy fsr --max-instances 100 --min-instances 0 --concurrency 1 \
    --image gcr.io/${PROJECT_ID}/demo-service:latest
  read k  

  echo "Execute ... hey -z 2m -c 20 -n 10 http://fsr.default.${DOMAINNAME}"
  echo "Open Cloudrun Console to see the results"
  hey -z 2m -c 20 -n 5 http://fsr.default.${DOMAINNAME}

  echo -n "Press any key to clean up the game"
  read k
  gcloud run services delete --quiet fsr     
}
source ./vars-anthos.sh
if [ ${ACTION} = "deploy" ]; then
  echo "deploy..."
  enable_services
  create_cluster
  register_cluster
  install_asm
  install_acm  
  enable_cloudrun
elif [ ${ACTION} = "delete" ]; then
  echo "delete anthos"
  CONTEXT="gke_${PROJECT_ID}_${ZONE}_${CLUSTER_NAME}"
  echo "use context: ${CONTEXT}"
  kubectl config use-context ${CONTEXT}
  delete_acm
  delete_asm
  unregister_cluster
  gcloud container clusters delete --quiet $CLUSTER_NAME
elif [ ${ACTION} = "add-domain" ]; then
  echo "Add default domain mapping ${DOMAINNAME} to the existing cluster"
  add_domain_mapping ${DOMAINNAME}
elif [ ${ACTION} = "enable-cloudrun-event" ]; then
  echo "Run Action: ${ACTION}"
  export namespace="event"
  export gcsname="${PROJECT_ID}"
  #enable_cloudrun_event
  set_broker
  bind_workload_identity
  link_gcs_cloudrun
elif [ ${ACTION} = "disable-cloudrun-event" ]; then
  echo "Run Action: ${ACTION}"
  export namespace="event"
  export gcsname="${PROJECT_ID}"
  disable_cloudrun_event
elif [ ${ACTION} = "demo-blue-green" ]; then
  echo "Demo CloudRun with Blue Green Deployment"
  set_repo_secret
  demo_blue_green
elif [ ${ACTION} = "demo-autoscale" ]; then
  echo "Demo AutoScaling with demo-service"
  set_repo_secret
  demo_autoscale
elif [ ${ACTION} = "demo-mlops" ]; then
  echo "Demo Captcha5-Custom using Torchserve framework..."
  SECRETNAME="gcr-secret"
  kubectl delete secret ${SECRETNAME}
  kubectl create secret docker-registry ${SECRETNAME} \
    --docker-server=https://gcr.io \
    --docker-username=_json_key \
    --docker-email=${ACCOUNT_EMAIL} \
    --docker-password="$(cat $SERVICE_KEY_PATH)"
  kubectl patch serviceaccount default \
    -p "{\"imagePullSecrets\": [{\"name\": \"$SECRETNAME\"}]}"
  echo -n "Press any key to deploy the inference model"
  read k
  gcloud run deploy fsr --image="gcr.io/${PROJECT_ID}/captcha5-custom:latest"
  echo -n "Test with image"
  echo -n "Press any key to clean up the inference model"
  read k
  gcloud run services delete --quiet fsr
fi
