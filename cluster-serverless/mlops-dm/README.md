# Generate Jupyter Playbook
This section shows how to use Deployment Manager to quick generate a Jupyter playbook.
Please use the following command:
```
gcloud deployment-manager deployments create notebook-1 --config ./create_notebook.yaml
```

