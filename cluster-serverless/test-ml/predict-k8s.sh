#!/bin/bash
if [ $# -eq 0 ]; then
	echo "No input file"
	test_file="SCN_202007311458_00036.png"
else
	test_file=$1
fi
MODEL_NAME=captcha5-custom
kubeflow_url="fsr.default.shawnk8s.com"
echo "convert file ${test_file} into input.json"
python img2bytearray.py ${test_file}
curl -v -X POST -H "Content-Type: application/json" http://${kubeflow_url}/predictions/${MODEL_NAME} -d @input.json; echo
