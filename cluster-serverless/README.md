# Serverless Use Cases Demonstration
## TL;DR
This project is used to demo serverless applications, mainly CloudRun on Anthos. I may add more demos for AppEngine or CloudFunction if there is a ticket. So...welcome to open me a case. 

The base of the demo is anthos-utility.sh which could be used to create anthos cluster. 

## Prerequisite
### CLI Installation
In this demo, hey (a.k.a apache benchmark CLI will be used to test cloudrun autoscaling capabilities). Please install or [download it](https://github.com/rakyll/hey) in advance. If you're using MacOS, you could use brew to install it.
```
brew install hey
```
### Environmental Variables
1. Set the following environmental variables
    ```
    # Script local - any string
    export ITERATION_SUFFIX="fellow-lab"
    export DOMAINNAME="shawnk8s.com"
    ```
2. Set docker repository credentials for Cloudrun
    Cloudrun uses default service account in kubernetes to pull the image. We therefore need to patch our docker credentials to it using the following secret.
    ```
    # In mlops-setup.sh 
    # set_repo_secret function
    SECRETNAME="gcr-secret"
    secret=$(kubectl get secret ${SECRETNAME})
    if [ -z "${secret}" ]; then
      kubectl create secret docker-registry $SECRETNAME \
        --docker-server=https://gcr.io \
        --docker-username=_json_key \
        --docker-email=${ACCOUNT_EMAIL} \
        --docker-password="$(cat ${SERVICE_KEY_PATH})"
      kubectl patch serviceaccount default \
        -p "{\"imagePullSecrets\": [{\"name\": \"$SECRETNAME\"}]}"   
    fi 
    ```

## mlops-setup.sh deploy
This command kicks off the installation
1. Create GKE clusters with workload identity and meshCA enabled. 
2. (trick) kubectl config rename-context will be used to change kubernetes context-name into cluster name
3. Register GKE cluster on Anthos hub. 
4. Install ACM on each of the GKE cluster. Enable both config syncer and open policy agent.
5. (optional) If ACM git repository was not produced in advance, please add config_acm call in the script (between install acm and install asm)
6. Download and install ASM
7. Enable Cloudrun on Anthos. Please note a new namespace "gke-system" with an external istio-ingress service would be produced. Following our guide, both ASM's istio-ingressgateway and istio-ingress external IP are able to connect to the cloudrun service. 

## mlops-setup.sh add-domain (Optional)
This step is optional. Cloudrun has a default FQDN naming rule "{servicename}.{namespace}.example.com". If you have a self-owned domain,  it is possible to replace it with the default "example.com" domain. Please do the following:
1. Replace the environment variable: DOMAINNAME with your own domain name
2. Execute the following command and an UI will be
```
mlops-setup.sh add-domain
```
![Domain Verifying](./images/verify-domain.png)
3. Please add a TXT entry in your DNS table with the blue-rectangled text in the above image. Cloudrun will verify the domain ownership with this action. 
![Domain Verified](./images/verified-success.png)

## mlops-setup.sh demo-blue-green
This command is to demonstrate how to conduct a blue-green deployment using Cloudrun. The weight ratio starts from 100/0, 75/25, and 0/100. Once the deployment completed, you will have a mario game to play.
```
Deploying new service... Done
  Creating Revision...
  Routing traffic...
Done. 
Service [fsr] revision [fsr-00001-yeh] has been deployed and is serving 100 percent of traffic.
Service URL: http://fsr.default.shawnk8s.com
```
![maxprime](images/maxprime.png)
```
Updating traffic... Done.
  Routing traffic...
Done. 
URL: http://fsr.default.shawnk8s.com
Traffic:
  100% LATEST (currently fsr-00002-ciw)
  Service fsr in namespace default
```
![mario](./images/mario.png)
## mlops-setup.sh demo-autoscale
This sample demonstrates autoscaling capability in Cloudrun. The container image was first created by cloudbuild and then deployed by cloudrun. When deploying with cloudrun, parameters were tuned to the concurrency to 1 to easily trigger the autoscaler.
```
 gcloud run deploy fsr --max-instances 100 --min-instances 0 --concurrency 1
```
hey CLI will be used to apply concurrent requests to the deployed service. Please use Cloudrun console to view the pod number increment and decrement during and after the generated requests.
![cloudrun](./images/cloudrun-console.png)

## mlops-setup.sh mlops
This sample demonstrates a captcha5 identification model using custom torchserve framework. The entire captcha5 source could be downloaded from [mlops-anthos](https://gitlab.com/shawnho1018/mlops-anthos). After the service deployed successfully, please use the following command to verify the Captcha5 result. 
```
test-ml/predict-k8s.sh test-ml/SCN_202007311458_00036.png

{
  "predictions": [
    "4R358"
  ]
* Connection #0 to host demo.default.shawnk8s.com left intact
}
```
It matches to the sample image ![SCN_202007311458_00036.png](./images/sample-1.png)