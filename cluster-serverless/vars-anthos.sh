#!/bin/bash
export PROJECT_ID="shawn-demo-2021"
export SERVICE_KEY_PATH=/Users/shawnho/workspace/gcp-keys/shawn-demo-2021-sa.key
export ACCOUNT_EMAIL=shawnho@google.com
export DOMAINNAME="shawnk8s.com"
export CLUSTER_NAME="cluster-demo-1"
export ZONE="asia-east1-a"
