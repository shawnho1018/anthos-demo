# Kubernetes Security (ACM & OPA) Demonstration
## TL;DR
This lab is to provide an end-to-end setup for Anthos install, config, and management (ICM), including GKE cluster creation, cluster registration, acm install & configuration, and asm installation. In addition to the Anthos ICM, this lab also served as a tutorial for running Open Policy Agent (OPA) in ACM to restrict unauthorized behavior in the cluster.

## Prerequisite
1. Setup one service account (e.g. asm_install) and provide the correponding IAM privilege (as shown below) to it. Also, add corresponding IAM privilege to the user account used on GCP console. 
![IAM Privilege](../cluster-mesh/images/IAMPermission.png "Required privileges")

It is also possible to combine all privileges into the service account and use service account as the installer. 
```
gcloud auth activate-service-account $SERVICE_ACCOUNT_NAME
```
This lab is for demo purpose and may not follow the minimal privilege. If you need better privileged definition, please open a ticket. 


2. Overwrite the following parameters.
These two parameters are required to be overwritten for a new deployment.
```
# any string to identify the cluster
export ITERATION_SUFFIX="100"
# Repo Name which will be further used for ACM
export CONFIG_REPO_NAME="config-repo"
```
The following parameters could be either provided by user or accept the default value. For a new deployment, please accept the default value as long as it's not conflicted to your existing environment.
```
# service account name (set in step 1) which will be used in the installation. Please pre-configure to account with the privilege above.
export SERVICE_ACCOUNT_NAME="anthos-install@${PROJECT_ID}.iam.gserviceaccount.com"
# service key location. Key will be automatically generated if it does not present.
export SERVICE_KEY_PATH=${HOME}/workspace/gcp-keys/${PROJECT_ID}-sa.key
# SSH-KEY used for git repository authentication
export SSH_KEY_PATH=${HOME}/.ssh/id_rsa
# BOTH CONFIG_REPO will be produced if CONFIG_REPO_NAME is provided
export CONFIG_REPO="ssh://shawnho@google.com@source.developers.google.com:2022/p/${PROJECT_ID}/r/${CONFIG_REPO_NAME}"
export CONFIG_REPO_PATH="${WORKDIR}/${CONFIG_REPO_NAME}"
```
## anthos-setup.sh create-repo
This command should be run if there is no git repository for ACM yet. It will 
1. Create an ssh private/public key pair.
2. Script will pause and hint you to attach the public key onto your cloud repository as shown below.
![Add Key](../cluster-mesh/image/add-ssh-public-key.png) 
3. Script will automatically create ACM folder and do the first commit into your cloud source repository. Please note that the created remote repository name is "google" and your master branch is "main".

## anthos-setup.sh delete-repo
1. Delete config-repo on Google cloud repository
2. Remove local repo folder if it exists.

## anthos-setup.sh deploy
This command kicks off the installation
1. Create GKE clusters with workload identity and meshCA enabled. 
2. (trick) kubectl config rename-context will be used to change kubernetes context-name into cluster name
3. Register GKE cluster on Anthos hub. 
4. Install ACM on each of the GKE cluster. Enable both config syncer and open policy agent.
5. (optional) If ACM git repository was not produced in advance, please add config_acm call in the script (between install acm and install asm)
6. Download and install ASM

## anthos-setup.sh delete
1. Uninstall ASM
2. Remove ACM
3. Unregister cluster from Anthos hub
4. Delete the entire cluster

## anthos-setup.sh demo
There are many useful examples [showed here](https://github.com/open-policy-agent/gatekeeper-library/tree/master/library). Running shell script to verify 3 OPA policies, including
1. Namespaces must include owner tag
2. Only allow a specified repository
3. Disallowed privileged container
Each function will be tested with a bad example first to allow user observe validation error in OPA. Then, a good example is applied to show the constraint can work if the condition fits. An sample result is shown below
```
# This is a sample to grant container repository
k8sallowedrepos.constraints.gatekeeper.sh/prod-repo-is-openpolicyagent created
Error from server ([denied by prod-repo-is-openpolicyagent] container <hello> has an invalid image repo <hello-world:latest>, allowed repos are ["gcr.io"]): error when creating "bad/hello-repo.yaml": admission webhook "validation.gatekeeper.sh" denied the request: [denied by prod-repo-is-openpolicyagent] container <hello> has an invalid image repo <hello-world:latest>, allowed repos are ["gcr.io"]
failed to apply
Press 'c' to continue
c
Continue from the program
pod/hello created
apply pod successfully
Press 'c' to continue
c
Continue from the program
pod "hello" deleted
k8sallowedrepos.constraints.gatekeeper.sh "prod-repo-is-openpolicyagent" deleted
```

## Notes:
When I wrote this tutorial, the successful platform combinations are:
* Kubernetes 1.18.12-gke.1210 (using Cilium)
* ACM 1.5.2
* ASM 1.8.3-asm.2