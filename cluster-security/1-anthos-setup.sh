#!/bin/bash
ACTION=$1
if [ -z "${ACTION}" ]; then
    echo "an 'ACTION' is required (deploy || delete || demo || create-repo || delete-repo)"
    exit 1
fi
source ./vars-anthos.sh
if [ ${ACTION} = "deploy" ]; then
  echo "deploy..."
  enable_services
  create_cluster
  register_cluster
  install_asm
  install_acm
elif [ ${ACTION} = "delete" ]; then
  echo "delete anthos"
  CONTEXT="gke_${PROJECT_ID}_${ZONE}_${CLUSTER_NAME}"
  echo "use context: ${CONTEXT}"
  original_context = $(kubectl config current-context)
  kubectl config use-context ${CONTEXT}
  delete_acm
  delete_asm
  unregister_cluster
  gcloud container clusters delete --quiet $CLUSTER_NAME
  kubectl config use-context ${original_context}
elif [ ${ACTION} = "create-repo" ]; then
  initialize
  source ${WORKDIR}/vars-${ITERATION_SUFFIX}.sh
  create_config_repo
elif [ ${ACTION} = "delete-repo" ]; then
  source ${WORKDIR}/vars-${ITERATION_SUFFIX}.sh
  delete_config_repo
elif [ ${ACTION} = "demo" ]; then
  echo "Demo Open Policy Agent"
  source ${WORKDIR}/vars-${ITERATION_SUFFIX}.sh
  pushd ${WORKDIR}
  cd demo-policy
  ./demo-bank.sh
  popd ${WORKDIR}
else
  echo "No action for it yet"
fi
