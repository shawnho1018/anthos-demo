#!/bin/bash
function waitloop() {
    echo "Press 'c' to continue"
    count=0
    while : ; do
        read -n 1 k <&1
        if [[ $k = 'c' ]] ; then
            printf "\nContinue from the program\n"
        break
        else
            ((count=$count+1))
            sleep 2
            printf "\nIterate for $count times\n"
            echo "\n"
            echo "Press 'c' to continue"
        fi
    done
}
echo "Here are all the sample constraint templates."
kubectl get constrainttemplates


# All namespace must have owner
function demo_namespace_owner() {
    echo "----------------------------------------------"
    echo "This is a scenario when an orphan namespace is left in the cluster."
    kubectl apply -f policy/owner-must-be-provided.yaml
    echo "Let's put in a owner-must-be-provided policy..."
    waitloop
    echo "Test with a wrong yaml..."
    cat bad/ns-shawn.yaml
    waitloop
    kubectl apply -f bad/ns-shawn.yaml
    echo "failed to apply"
    waitloop

    echo "Test with a good yaml..."
    cat good/ns-shawn.yaml
    waitloop
    kubectl apply -f good/ns-shawn.yaml
    echo "apply ns successfully"
    waitloop
    kubectl delete -f good/ns-shawn.yaml
    kubectl delete -f policy/owner-must-be-provided.yaml
}
function demo_disallow_privileged_pod() {
    echo "----------------------------------------------"
    echo "Some developers tries to use privileged container without notifying security team..."
    # no privileged container
    echo "Let's put in a disallowed privilege policy..."
    kubectl apply -f policy/disallow-privileged-container.yaml
    waitloop
    echo "Test with a wrong yaml..."
    cat bad/privileged-pod.yaml
    waitloop
    kubectl apply -f bad/privileged-pod.yaml
    echo "failed to apply"
    waitloop
    echo "Test with a good yaml"
    cat good/nginx-pod.yaml
    waitloop
    kubectl apply -f good/nginx-pod.yaml
    echo "apply pod successfully"
    waitloop

    kubectl delete -f good/nginx-pod.yaml
    kubectl delete -f policy/disallow-privileged-container.yaml
}
function demo_allow_repo() {
    echo "----------------------------------------------"
    echo "Some developers forgets to put registry name or even use un-verified repository"
    echo "Let's put a policy to STOP it..."
    kubectl apply -f policy/allow-repo.yaml
    waitloop
    echo "Test with a wrong yaml..."
    cat bad/hello-repo.yaml
    waitloop
    kubectl apply -f bad/hello-repo.yaml
    echo "failed to apply"
    waitloop
    echo "Test with a good yaml"
    cat good/hello-repo.yaml
    waitloop
    kubectl apply -f good/hello-repo.yaml
    echo "apply pod successfully"
    waitloop   

    kubectl delete -f good/hello-repo.yaml
    kubectl delete -f policy/allow-repo.yaml
}
demo_namespace_owner
sleep 3
demo_allow_repo
sleep 3
demo_disallow_privileged_pod

