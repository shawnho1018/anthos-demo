
#!/bin/bash
source ./user-setting.sh
gcloud iam service-accounts create anthos-install
gcloud projects add-iam-policy-binding ${PROJECT_ID} \
--member=serviceAccount:anthos-install@${PROJECT_ID}.iam.gserviceaccount.com \
--role=roles/owner

mkdir -p ~/workspace/gcp-keys
gcloud iam service-accounts keys create ~/workspace/gcp-keys/${PROJECT_ID}-sa.key --iam-account=anthos-install@${PROJECT_ID}.iam.gserviceaccount.com
