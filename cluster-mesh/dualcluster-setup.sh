#!/bin/bash
function initialize() {
  if [ -z ${ITERATION_SUFFIX} ]; then
    echo "Please provide ITERATION_SUFFIX variable"
    return 1
  fi 
  if [ -z ${WORKDIR} ]; then
    echo "Please provide WORKDIR variable"
    return 1
  fi 
  if [ -z ${CONFIG_REPO_NAME} ]; then
    echo "Please provide CONFIG_REPO_NAME variable"
    return 1
  fi
  export PROJECT_ID=$(gcloud config list --format 'value(core.project)')
  export ACCOUNT_EMAIL="shawnho@google.com"
  export SERVICE_KEY_PATH=${HOME}/workspace/gcp-keys/${PROJECT_ID}-sa.key
  export SERVICE_ACCOUNT_NAME="anthos-install@${PROJECT_ID}.iam.gserviceaccount.com"
  if [ ! -f ${SERVICE_KEY_PATH} ]; then
    echo "key has not downloaded yet. Create service key on user's behalf..."
    gcloud iam service-accounts keys create --iam-account ${SERVICE_ACCOUNT_NAME} ${SERVICE_KEY_PATH}
  fi

cat <<EOT > ${WORKDIR}/vars-${ITERATION_SUFFIX}.sh
export WORKDIR=$(pwd)
export REGIONAL="false"
export CLUSTER_NAME_1="cluster-${ITERATION_SUFFIX}-1"
export CLUSTER_NAME_2="cluster-${ITERATION_SUFFIX}-2"
export PROJECT_ID=$(gcloud config list --format 'value(core.project)')
export PROJECT_NUMBER=$(gcloud projects describe ${PROJECT_ID} --format 'value(projectNumber)')
export REGION=$(gcloud config get-value compute/region 2> /dev/null)
export ZONE=$(gcloud config get-value compute/zone 2> /dev/null)
export ACCOUNT_EMAIL=${ACCOUNT_EMAIL}

# service account name which will be used in the installation. Please pre-configure to account with the privilege above.
export SERVICE_ACCOUNT_NAME="anthos-install@${PROJECT_ID}.iam.gserviceaccount.com"
# service key location. Key will be automatically generated if it does not present.
export SERVICE_KEY_PATH=${HOME}/workspace/gcp-keys/${PROJECT_ID}-sa.key
# SSH-KEY used for git repository authentication
export SSH_KEY_PATH=${HOME}/.ssh/id_rsa
# ACM remote repo fqdn
export CONFIG_REPO="ssh://${ACCOUNT_EMAIL}@source.developers.google.com:2022/p/${PROJECT_ID}/r/config-repo"
# ACM local repo folder path. Please notice that this folder only be created if config-acm was executed. 
export CONFIG_REPO_PATH="${WORKDIR}/config-repo"

export VPC="demo"
export SUBNET_1="demo-subnet-1"
export SUBNET_2="demo-subnet-2"
export SUBNET_1_RANGE="192.168.0.0/16"
export SUBNET_2_RANGE="192.169.0.0/16"
EOT
}
function enable_services() {
    gcloud services enable \
    container.googleapis.com \
    compute.googleapis.com \
    monitoring.googleapis.com \
    logging.googleapis.com \
    meshca.googleapis.com \
    meshtelemetry.googleapis.com \
    meshconfig.googleapis.com \
    anthos.googleapis.com \
    gkeconnect.googleapis.com \
    gkehub.googleapis.com \
    multiclusteringress.googleapis.com  
}

function create_networks() {
    echo "detect if vpc exists: ${VPC}"
    is_vpc_non_exist=$(gcloud compute networks list --filter=name=${VPC})
    if [[ -z ${is_vpc_non_exist} ]]; then
        echo "creating vpc accordingly..."
        gcloud compute networks create ${VPC} --subnet-mode=custom
	gcloud compute firewall-rules create "${VPC}-mesh" --network ${VPC} --allow tcp,udp,icmp --source-ranges "0.0.0.0/0"
    else 
        echo "vpc exists...no creation is needed..."
    fi
    echo "detect if subnet ${SUBNET_1} exists..."
    
    for sub in $SUBNET_1 $SUBNET_2; do
        is_subnet_non_exist=$(gcloud compute networks subnets list --filter="name:${sub} region:${REGION} network:${VPC}")
        if [[ -z ${is_subnet_non_exist} ]]; then
            echo "creating subnet accordingly"
            assigned_range=${SUBNET_2_RANGE}
            if [ "${sub}" = "${SUBNET_1}" ]; then
                assigned_range="${SUBNET_1_RANGE}"               
            fi
            gcloud compute networks subnets create ${sub} --network ${VPC} --region ${REGION} --range ${assigned_range}
        else
            echo "subnet exists...no creation is needed..."
        fi
    done
}

function delete_networks() {
    for sub in ${SUBNET_1} ${SUBNET_2}; do
        gcloud compute networks subnets delete --quiet ${sub}
    done
    gcloud compute firewall-rules delete --quiet \
      $(gcloud compute firewall-rules list | grep ${VPC} | awk '{print $1}')
    gcloud compute networks delete --quiet ${VPC}
}

function create_cilium_cluster() {
  if [ $# = 2 ]; then
    LOCAL_CLUSTER_NAME=$1
    LOCAL_SUBNET_NAME=$2
  else 
    >&2 echo "Error calling create_cilium_cluster. Please provide two variables, cluster_name and subnet_name"
    exit 1;
  fi

  local CLUSTER_NON_EXIST=$(gcloud container clusters describe "${LOCAL_CLUSTER_NAME}" --format="value(name)" 2> /dev/null)
  if [ -z "${CLUSTER_NON_EXIST}" ]; then
    # Cluster does not exist
    echo "Create clusters at ${PROJECT_ID} Project"
    echo "Creating a new GKE Cluster...(may take 3-10 minutes)"
    echo "Test Cilium as the CNI"
    gcloud beta container clusters create ${LOCAL_CLUSTER_NAME} \
        --release-channel stable \
        --enable-stackdriver-kubernetes \
        --enable-dataplane-v2 \
        --enable-ip-alias \
        --workload-pool=${PROJECT_ID}.svc.id.goog \
        --enable-autoscaling --min-nodes 1 --max-nodes 4 --num-nodes 3 \
        --machine-type=n1-standard-4 \
        --labels mesh_id="proj-${PROJECT_NUMBER}" \
        --scopes=cloud-platform \
        --quiet \
        --zone ${ZONE} \
        --network ${VPC} \
        --subnetwork ${LOCAL_SUBNET_NAME}
    echo -e "${FANCY_OK} Cluster created, use this command to create your 'kubectl' context configuration:"
  else
    echo -e "${FANCY_NEUTRAL} Cluster already exists, moving on"
  fi
  echo "Re-connect to cluster:  gcloud container clusters get-credentials ${LOCAL_CLUSTER_NAME} --zone ${ZONE} --project ${PROJECT_ID}"
  gcloud container clusters get-credentials ${LOCAL_CLUSTER_NAME} --zone ${ZONE} --project ${PROJECT_ID}
  result=$(kubectl get clusterrolebinding anthos-admin)
  if [[ -z ${result} ]]; then
    echo "Add cluster-admin role for GCP user account"
    kubectl create clusterrolebinding anthos-admin --clusterrole=cluster-admin --user ${ACCOUNT_EMAIL} --context ${LOCAL_CLUSTER_NAME}
  fi
  kubectl config rename-context gke_${PROJECT_ID}_${ZONE}_${LOCAL_CLUSTER_NAME} ${LOCAL_CLUSTER_NAME}
}

function register_cluster() {
  if [ $# = 1 ]; then
    LOCAL_CLUSTER_NAME=$1
  else 
    >&2 echo "Error calling cluster registration. Please provide cluster_name only."
    exit 1;
  fi

  if [ ! -f "${SERVICE_KEY_PATH}" ]; then
    gcloud iam service-accounts keys create --iam-account ${SERVICE_ACCOUNT_NAME} ${SERVICE_KEY_PATH}
  else
    echo "service key exists - use the existing key"
  fi  
  if [[ ${REGIONAL} = "true" ]]; then
    echo "registering regional cluster..."
    gcloud container hub memberships register ${LOCAL_CLUSTER_NAME} --gke-cluster "${REGION}/${LOCAL_CLUSTER_NAME}" --service-account-key-file="${SERVICE_KEY_PATH}"
  else 
    echo "registering zonal cluster..."
    gcloud container hub memberships register ${LOCAL_CLUSTER_NAME} --gke-cluster "${ZONE}/${LOCAL_CLUSTER_NAME}" --service-account-key-file="${SERVICE_KEY_PATH}"
  fi
}
function unregister_cluster() {
  if [ $# = 1 ]; then
    LOCAL_CLUSTER_NAME=$1
  else 
    >&2 echo "Error calling cluster registration. Please provide cluster_name only."
    exit 1;
  fi

  if [ ${REGIONAL} = "true" ]; then
    gcloud container hub memberships unregister --quiet ${LOCAL_CLUSTER_NAME} --gke-cluster="${REGION}/${LOCAL_CLUSTER_NAME}"
  else 
    gcloud container hub memberships unregister --quiet ${LOCAL_CLUSTER_NAME} --gke-cluster="${ZONE}/${LOCAL_CLUSTER_NAME}"
  fi
}
function install_acm() {
  echo "config-acm"
  if [ $# = 1 ]; then
    LOCAL_CLUSTER_NAME=$1
  else 
    >&2 echo "Error calling cluster registration. Please provide cluster_name only."
    exit 1;
  fi
  gcloud alpha container hub config-management enable
  kubectl create ns config-management-system --context ${LOCAL_CLUSTER_NAME}
  gsutil cp gs://config-management-release/released/latest/config-sync-operator.yaml config-sync-operator.yaml
  kubectl apply -f config-sync-operator.yaml --context ${LOCAL_CLUSTER_NAME}
  kubectl create secret generic git-creds \
    --namespace=config-management-system \
    --from-file=ssh=${SSH_KEY_PATH} \
    --context ${LOCAL_CLUSTER_NAME}
cat <<- EOT > ${WORKDIR}/config-management.yaml
apiVersion: configmanagement.gke.io/v1
kind: ConfigManagement
metadata:
  name: config-management
spec:
  clusterName: ${LOCAL_CLUSTER_NAME}
  git:
    syncRepo: ${CONFIG_REPO}
    syncBranch: main
    secretType: ssh
EOT
   kubectl apply -f ${WORKDIR}/config-management.yaml --context=${LOCAL_CLUSTER_NAME}
}

function delete_acm() {
  if [ $# = 1 ]; then
    LOCAL_CLUSTER_NAME=$1
  else 
    >&2 echo "Error calling cluster registration. Please provide cluster_name only."
    exit 1;
  fi    
cat <<- EOT > ${WORKDIR}/config-management.yaml
apiVersion: configmanagement.gke.io/v1
kind: ConfigManagement
metadata:
  name: config-management
spec:
  git:
   syncRepo: ${CONFIG_REPO}
   syncBranch: main
   secretType: ssh
EOT
  kubectl delete configmanagement --all --context=${LOCAL_CLUSTER_NAME}
  kubectl delete crd configmanagements.configmanagement.gke.io --context=${LOCAL_CLUSTER_NAME}
  kubectl -n kube-system delete all -l k8s-app=config-management-operator --context=${LOCAL_CLUSTER_NAME}
  kubectl delete ns config-management-system --context=${LOCAL_CLUSTER_NAME}
}
function install_asm() {
  if [ $# = 1 ]; then
    LOCAL_CLUSTER_NAME=$1
  else 
    >&2 echo "Error calling cluster registration. Please provide cluster_name only."
    exit 1;
  fi      
  if [[ ! -f install_asm ]]; then
    curl https://storage.googleapis.com/csm-artifacts/asm/install_asm_1.8 > install_asm
    chmod +x install_asm
  fi
  if [[ -d "asm_install" ]]; then
     rm -rf asm_install
  fi

  mkdir -p asm_install 
  # create cluster-admin rolebinding with user
  kubectl create clusterrolebinding istio-admin --clusterrole=cluster-admin \
    --user ${SERVICE_ACCOUNT_NAME} --context $LOCAL_CLUSTER_NAME
  LOCATION=${ZONE}
  if [ ${REGIONAL} = "true" ]; then
    LOCATION=${REGION}
  fi
  ./install_asm -v --project_id ${PROJECT_ID} \
    --cluster_name ${LOCAL_CLUSTER_NAME} \
    --cluster_location ${LOCATION} \
    -s ${SERVICE_ACCOUNT_NAME} \
    -k ${SERVICE_KEY_PATH} \
    --mode install \
    --output_dir asm_install \
    --enable_gcp_iam_roles \
    --enable_gcp_apis \
    --enable_gcp_components \
    --enable_cluster_roles \
    --enable_cluster_labels
}

function config_asm() {
# create endpoint discovery between clusters
  istioctl x create-remote-secret --context=${CLUSTER_NAME_1} --name=${CLUSTER_NAME_1} | \
    kubectl apply -f - --context=${CLUSTER_NAME_2}
  istioctl x create-remote-secret --context=${CLUSTER_NAME_2} --name=${CLUSTER_NAME_2} | \
    kubectl apply -f - --context=${CLUSTER_NAME_1}
  mytag=$(kubectl get deployments -n istio-system -l app=istiod --context ${CLUSTER_NAME_1} --show-labels \
   | awk '{print $6}' | sed $'s/,/\\n/g' | grep "istio.io/rev")
   echo "AutoInjection tag is: ${mytag}"
}

function deploy_testapp() {
  mytag=$(kubectl get deployments -n istio-system -l app=istiod --context ${CLUSTER_NAME_1} --show-labels \
   | awk '{print $6}' | sed $'s/,/\\n/g' | grep "istio.io/rev") 
  PROJECT_DIR="${WORKDIR}/asm_install/istio-1.8.3-asm.2"
  for c in ${CLUSTER_NAME_1} ${CLUSTER_NAME_2}; do 
    kubectl create --context=${c} namespace sample
    kubectl label --context=${c} namespace sample ${mytag}
    kubectl create --context=${c} \
       -f ${PROJECT_DIR}/samples/helloworld/helloworld.yaml \
       -l service=helloworld -n sample
  done 
   # deploy different version in separate cluster
  kubectl create --context=${CLUSTER_NAME_1} \
    -f ${PROJECT_DIR}/samples/helloworld/helloworld.yaml \
    -l version=v1 -n sample

  kubectl create --context=${CLUSTER_NAME_2} \
    -f ${PROJECT_DIR}/samples/helloworld/helloworld.yaml \
    -l version=v2 -n sample  
  sed -i.bk 's/host: helloworld/host: helloworld.sample.svc.cluster.local/g' ${PROJECT_DIR}/samples/helloworld/helloworld-gateway.yaml
  kubectl apply --context=${CLUSTER_NAME_2} \
    -f ${PROJECT_DIR}/samples/helloworld/helloworld-gateway.yaml
  ipaddr=$(kubectl get svc -n istio-system -l app=istio-ingressgateway --context=${CLUSTER_NAME_2} | awk 'NR>1 {print $4}')
  while true; do
    curl ${ipaddr}/hello;
    sleep 1;
  done
}

function delete_testapp() {
  PROJECT_DIR="${WORKDIR}/asm_install/istio-1.8.3-asm.2"
  kubectl delete --context=${CLUSTER_NAME_2} \
    -f ${PROJECT_DIR}/samples/helloworld/helloworld-gateway.yaml
  for c in ${CLUSTER_NAME_1} ${CLUSTER_NAME_2}; do 
    kubectl delete --context=${c} namespace sample
  done
  mv ${PROJECT_DIR}/samples/helloworld/helloworld-gateway.yaml.bk ${PROJECT_DIR}/samples/helloworld/helloworld-gateway.yaml
}

function delete_asm() {
  if [ $# = 1 ]; then
    LOCAL_CLUSTER_NAME=$1
  else 
    >&2 echo "Error calling cluster registration. Please provide cluster_name only."
    exit 1;
  fi      
  istioctl manifest generate --set profile=asm-gcp --context ${LOCAL_CLUSTER_NAME} \
   | kubectl delete --ignore-not-found=true --context ${LOCAL_CLUSTER_NAME} -f -
  kubectl delete ns istio-system --context ${LOCAL_CLUSTER_NAME}
}

ACTION=$1
if [ -z "${ACTION}" ]; then
    echo "an 'ACTION' is required (deploy || delete || demo || remove-demo)"
    exit 1
fi
# Script local - any string
export ITERATION_SUFFIX="201"
# Repo Name which will be further used for ACM.
# It is recommended NOT ALTER the config-repo name. 
# If the name needs to be altered, please rename the \
# cloned sub-folder (config-repo) to use the same folder name as the name here.
export CONFIG_REPO_NAME="config-repo"
export WORKDIR=$(pwd)

if [ ${ACTION} = "deploy" ]; then
  # Script local - 3-digit suffix
  echo "deploy dual clusters..."
  enable_services
  initialize
  source ${WORKDIR}/vars-${ITERATION_SUFFIX}.sh
  create_networks
  create_cilium_cluster $CLUSTER_NAME_1 $SUBNET_1
  create_cilium_cluster $CLUSTER_NAME_2 $SUBNET_2
  for cluster in ${CLUSTER_NAME_1} ${CLUSTER_NAME_2}; do
    register_cluster $cluster
    install_asm $cluster
    install_acm $cluster
  done
  config_asm
elif [ ${ACTION} = "delete" ]; then
  echo "delete dual clusters"
  source ${WORKDIR}/vars-${ITERATION_SUFFIX}.sh
  for cluster in ${CLUSTER_NAME_1} ${CLUSTER_NAME_2}; do
    delete_acm $cluster
    delete_asm $cluster
    unregister_cluster $cluster    
    gcloud container clusters delete --quiet $cluster
  done
  delete_networks
elif [ ${ACTION} = "demo" ]; then
  echo "test application"
  source ${WORKDIR}/vars-${ITERATION_SUFFIX}.sh
  deploy_testapp
elif [ ${ACTION} = "remove-demo" ]; then
  echo "remove application"
  source ${WORKDIR}/vars-${ITERATION_SUFFIX}.sh
  delete_testapp  
else
  echo "No action for it yet"
fi
