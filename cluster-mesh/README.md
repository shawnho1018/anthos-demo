# Cross Cluster Istio Traffic Demonstration
## TL;DR
This project is to demonstrate how to utilize istio to produce cross cluster traffic. Two separate subnets and clusters will be created using the same MeshCA and configured against anthos, acm, and asm. Then, config_asm function exchanges neighboring kubeconfig information to have istio control plane capable of speaking to neighboring cluster's kube-apiserver. This step creates mesh between the two clusters.

We also implement test function to quickly deploy different version of the software on each cluster. 
![Lab Diagram](images/architecture.png)
The entire script consists of these installation steps: 
## Prerequisite
The following variables are required to be replaced by user:
1. Setup one service account (e.g. asm_install) and provide the correponding IAM privilege (as shown below) to it. Also, add corresponding IAM privilege to the user account used on GCP console. 
![IAM Privilege](images/IAMPermission.png "Required privileges")

It is also possible to combine all privileges into the service account and use service account as the installer. 
```
gcloud auth activate-service-account $SERVICE_ACCOUNT_NAME
```
This lab is for demo purpose and may not follow the minimal privilege. If you need better privileged definition, please open a ticket. 


2. Overwrite the following parameters.
```
# any 3 digit index number to identify the cluster
export ITERATION_SUFFIX="201"
# service account name (set in step 1) which will be used in the installation. Please pre-configure to account with the privilege above.
export SERVICE_ACCOUNT_NAME="anthos-install@${PROJECT_ID}.iam.gserviceaccount.com"
# service key location. Key will be automatically generated if it does not present.
export SERVICE_KEY_PATH=${HOME}/workspace/gcp-keys/${PROJECT_ID}-sa.key
# SSH-KEY used for git repository authentication
export SSH_KEY_PATH=${HOME}/.ssh/id_rsa
# ACM remote repo fqdn
export CONFIG_REPO="ssh://shawnho@google.com@source.developers.google.com:2022/p/shawn-demo-2021/r/config-repo"
# ACM local repo folder path. Please notice that this folder only be created if config-acm was executed. 
export CONFIG_REPO_PATH="${WORKDIR}/config-repo"
```
3. Create ACM REPO 
If this is the first time, you're trying to config ACM. Please go to ../cluster-security folder to run
```
# create a source repository under the project and also do a first commit to the repository
anthos-setup.sh create-repo
```

## Installation
### dualcluster_setup.sh deploy
This command kicks off the installation
1. Create a VPC with 2 subnets (each subnet for 1 cluster)
2. Create GKE clusters with workload identity and meshCA enabled. 
3. (trick) kubectl config rename-context will be used to change kubernetes context-name into cluster name
4. Register GKE cluster on Anthos hub. 
5. Install ACM on each of the GKE cluster. 
6. (optional) If ACM git repository was not produced in advance, please add config_acm call in the script (between install acm and install asm)
7. Download and install ASM
8. Config mutual trust ASM for cross cluster traffic.

## dualcluster_setup.sh delete
This command is reponsible for the entire resource deletion, including
1. Uninstall ASM
2. Remove ACM
3. Unregister cluster from Anthos hub
4. Delete the entire cluster
5. Delete 2 subnets and vpc

## dualcluster_setup.sh test
This command could deploy helloworld workloads version 1 and 2 into separate clusters. It will automatically run curl command to show workloads being load balanced onto separate cluster.

## dualcluster_setup.sh remove
This command could remove helloworld workloads. 

## Notes:
When I wrote this tutorial, the successful platform combinations are:
* Kubernetes 1.18.12-gke.1210 (using Cilium)
* ACM 1.5.2
* ASM 1.8.3-asm.2

