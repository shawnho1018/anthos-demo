#!/bin/zsh
for m in gke hub-primary asm-primary acm-primary; do
  terraform apply -auto-approve -target module.${m} -var-file ./demo.tfvars
done
terraform apply -auto-approve -target null_resource.get-context -var-file ./demo.tfvars
terraform apply -auto-approve -target module.app-workload-identity -var-file ./demo.tfvars
