output "external-ip" {
  value = data.external.istiogateway.result.external_ip
  description = "istio-ingressgateway IP"
}
