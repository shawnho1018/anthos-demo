# Deploy microservices into GKE cluster
resource "null_resource" "get-context" {
  provisioner "local-exec" {
      command = "gcloud container clusters get-credentials ${var.cluster_name} --zone ${var.location}"
  }
  depends_on = [google_container_cluster.primary]
}
output "endpoint" {
    value = "google_container_cluster.primary.endpoint"
}
output "location" {
    value = "google_container_cluster.primary.location"
}
output "name" {
    value = "google_container_cluster.primary.name"
}