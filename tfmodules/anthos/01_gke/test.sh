#!/bin/zsh
counter=10
tag=$(date +%s)
cat << EOT > ./log-tester.yaml
apiVersion: batch/v1
kind: Job
metadata:
  name: logging-${tag}
spec:
  template:
    metadata:
      labels:
        app: tester
    spec:
      containers:
      - image: ubuntu
        name: ubuntu
        command: ['/bin/bash', '-c', '--']
        args: ['for c in `seq 1 $counter`; do echo "hello ${c}"; sleep 1; done']
      restartPolicy: Never
  backoffLimit: 4
EOT
kubectl apply -f ./log-tester.yaml
sleep 30
success=$(gcloud logging read "resource.labels.namespace_name="default" AND resource.labels.cluster_name="cluster-primary" AND labels."k8s-pod/job-name"="logging-${tag}" AND labels."k8s-pod/app"="tester"" | grep 'hello' | wc -l)
kubectl delete -f ./log-tester.yaml
json_obj='{"success": "%s", "test": "%s"}'
printf "$json_obj" "$success" "$counter"