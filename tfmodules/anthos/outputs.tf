output "project" {
  value = var.project_id
}
output "project_number" {
  value = data.google_project.project.number
}
