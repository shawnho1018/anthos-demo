module "hub-primary" {
  source           = "terraform-google-modules/kubernetes-engine/google//modules/hub"
  project_id       = var.project_id
  cluster_name     = var.cluster_name
  location         = var.primary_zones[0] 
  cluster_endpoint = module.gke.endpoint
  gke_hub_membership_name = var.cluster_name
  use_existing_sa = true
  sa_private_key  = base64encode(data.local_file.gcpkey.content)
}

resource "null_resource" "install-asm" {
  provisioner "local-exec" {
    command = <<-EOT
      gsutil cp gs://csm-artifacts/asm/install_asm_1.8 ./install_asm
      chmod +x ./install_asm
      mkdir -p ./asm_install
      cp prometheus-metrics-export.yaml asm_install/asm/istio/options/
      ./install_asm -v --project_id ${var.project_id} \
      --cluster_name ${var.cluster_name} --cluster_location ${var.primary_zones[0]} \
      -s anthos-install@${var.project_id}.iam.gserviceaccount.com -k ${var.gcp_key_path} \
      --mode install --output-dir asm_install --enable_cluster_labels --enable_cluster_roles \
      --option prometheus-metrics-export 
      sleep 3
      kubectl apply -f asm_install/asm/third-party/prometheus.yaml
    EOT
    on_failure = "fail"
  }
  provisioner "local-exec" {
    when    = destroy
    command = <<-EOT
       istioctl manifest generate --set profile=asm-gcp | kubectl delete --ignore-not-found=true -f -
       sleep 10
       rm -rf asm_install/
       kubectl delete ns istio-system
    EOT
    on_failure = "continue"
  }
}
resource "null_resource" "patch-ip" {
  provisioner "local-exec" {
    command = <<-EOT
      echo "patch loadbalancerIP: ${var.loadBalancerIP}" 
      kubectl patch svc istio-ingressgateway -n istio-system --type="json" -p '[{"op": "add", "path": "/spec/loadBalancerIP", "value": "${var.loadBalancerIP}"}]'
    EOT
  }
}
module "asm-primary" {
  source           = "terraform-google-modules/kubernetes-engine/google//modules/asm"
  version          = "14.3.0"
  project_id       = var.project_id
  cluster_name     = var.cluster_name
  asm_version      = 1.8
  location         = var.primary_zones[0]
  cluster_endpoint = module.gke.endpoint
  asm_dir          = "asm-${var.cluster_name}"
}

module "acm-primary" {
  source           = "github.com/terraform-google-modules/terraform-google-kubernetes-engine//modules/acm"
  project_id       = var.project_id
  cluster_name     = var.cluster_name
  location         = var.primary_zones[0]
  cluster_endpoint = module.gke.endpoint

  operator_path    = "scripts/config-management-operator.yaml"
  sync_repo        = var.acm_repo_location
  sync_branch      = var.acm_branch
  ssh_auth_key     = file("${path.root}/${var.ssh_auth_key_path}")

  enable_policy_controller = true
}

module "app-workload-identity" {
  source     = "terraform-google-modules/kubernetes-engine/google//modules/workload-identity"
  name       = "terraform-admin"
  namespace  = "default"
  project_id = var.project_id
  roles = ["roles/owner", "roles/storage.admin"]
  depends_on = [module.hub-primary.wait]
}
