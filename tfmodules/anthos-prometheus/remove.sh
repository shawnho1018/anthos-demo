#!/bin/zsh
for m in app-workload-identity acm-primary hub-primary gke; do
  terraform destroy -auto-approve -target module.${m} -var-file ./demo.tfvars
done
