data "local_file" "gcpkey" {
  filename = "${var.gcp_key_path}"
}

data "google_project" "project" {
}

terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "3.68.0"
    }

    google-beta = {
      source = "hashicorp/google-beta"
      version = "3.68.0"
    }
    kubernetes  = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.0.0"
    }
  }
}

provider "google" {
  project = var.project_id
  region  = var.primary_region
  credentials = data.local_file.gcpkey.content
}

provider "google-beta" {
  project = var.project_id
  region  = var.primary_region
  credentials = data.local_file.gcpkey.content
}

provider "kubernetes" {
  config_path = "~/.kube/config"
}
