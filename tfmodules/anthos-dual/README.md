# Introduction
ASM has released managed mode which helps istio users managing the complicated istiod (control plane). Some people may have doubts if managed ASM could also achieve cross-cluster traffic routing. This project is a terraform sample which could quickly be applied to run. This code has been tested under ASM 1.9.3-asm.2 with two separate clusters (one in asia-east1; the other in asia-northeast1). My test client is MacOS. 

## Prerequisite
Here are the list of CLI which requires to be installed in advance. 
* [Terraform CLI](https://www.terraform.io/downloads.html): Tested with v0.14.10
* [istioctl](https://cloud.google.com/service-mesh/docs/downloading-istioctl): Tested against 1.9.3-asm.2.

Also, we need a service-account key. Since this is a test program, please provide project.owner privilege for this service account. The path of the key file is required in variable.tf.

## Deploy
* Please modify the variable.tf accordingly before proceeding.
* run install.sh

Note that helloworld services will be deployed on sample namespace in both clusters.

## Test
We'll deploy helloworld v1 version in cluster 1, and v2 version in cluster 2. With the cross-cluster capability, whether client apps connect to the istio-ingressgateway in either cluster 1 or 2, they should see v1 and v2 pods are both providing services. 

After the deployment successful, please find the istio-ingressgateway loadbalancer IP address in cluster 1 and run the following scripts
```
while true; do curl [istio-ingress-ip]/hello; sleep 1; done
```
For example, my results are like
```
shawnho@shawnho-macbookpro% while true; do curl 35.200.14.188/hello; sleep 1; done
Hello version: v1, instance: helloworld-v1-5b75657f75-v4z4d
Hello version: v2, instance: helloworld-v2-7855866d4f-jt58j
Hello version: v2, instance: helloworld-v2-7855866d4f-jt58j
Hello version: v2, instance: helloworld-v2-7855866d4f-jt58j
Hello version: v1, instance: helloworld-v1-5b75657f75-v4z4d
Hello version: v2, instance: helloworld-v2-7855866d4f-jt58j
Hello version: v1, instance: helloworld-v1-5b75657f75-v4z4d
Hello version: v1, instance: helloworld-v1-5b75657f75-v4z4d
```