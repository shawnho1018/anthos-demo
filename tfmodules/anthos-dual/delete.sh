#!/bin/zsh
varfile="shawn.tfvars"

echo "Delete Managed ASM to both clusters"
terraform destroy -target module.asm-primary -auto-approve -var-file ${varfile}
terraform destroy -target module.asm-secondary -auto-approve -var-file ${varfile}

echo "Delete GKE clusters"
terraform destroy -target module.cluster-primary -auto-approve -var-file ${varfile}
terraform destroy -target module.cluster-secondary -auto-approve -var-file ${varfile}

