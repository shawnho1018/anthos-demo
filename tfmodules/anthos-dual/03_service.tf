
resource "null_resource" "deploy_hello" {
  provisioner "local-exec" {
    command = <<-EOT
    echo "apply services to context ${local.context1}"
    kubectl create --context ${local.context1} ns sample
    kubectl label  --context ${local.context1} namespace sample istio.io/rev=asm-managed
    kubectl apply  --context=${local.context1} -f ${path.module}/kubernetes-manifests/helloworld/helloworld.yaml -l service=helloworld -n sample
    kubectl apply  --context ${local.context1} -f ${path.module}/kubernetes-manifests/helloworld/helloworld.yaml -l version=v1 -n sample
    kubectl apply  --context ${local.context1} -f ${path.module}/kubernetes-manifests/helloworld/helloworld-gateway.yaml -n sample
    echo "apply services to context ${local.context2}"
    kubectl create --context ${local.context2} ns sample
    kubectl label  --context ${local.context2} namespace sample istio.io/rev=asm-managed
    kubectl apply  --context=${local.context2} -f ${path.module}/kubernetes-manifests/helloworld/helloworld.yaml -l service=helloworld -n sample
    kubectl apply  --context ${local.context2} -f ${path.module}/kubernetes-manifests/helloworld/helloworld.yaml -l version=v2 -n sample
    kubectl apply  --context ${local.context2} -f ${path.module}/kubernetes-manifests/helloworld/helloworld-gateway.yaml -n sample
  EOT
  }
  triggers = {
    "before" = module.asm-secondary.asm_wait
  }
}
