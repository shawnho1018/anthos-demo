#!/bin/zsh
varfile="shawn.tfvars"
function Message(){
    echo "$1"
    echo "Press any key to continue..."
    read -n 1
}
terraform init
terraform plan -var-file ${varfile}

echo "Enable Project Services"
terraform apply -target module.project-services -auto-approve -var-file ${varfile}

echo "Deploy GKE clusters"
terraform apply -target module.cluster-primary -auto-approve -var-file ${varfile}
terraform apply -target module.cluster-secondary -auto-approve -var-file ${varfile}

echo "Deploy Managed ASM to both clusters"
terraform apply -target module.asm-primary -auto-approve -var-file ${varfile}
terraform apply -target module.asm-secondary -auto-approve -var-file ${varfile}

echo "Configure cross-cluster capability and istio-gateway"
terraform apply -target null_resource.trust-establish -auto-approve -var-file ${varfile}
terraform apply -target module.gateway-primary -auto-approve -var-file ${varfile}
terraform apply -target module.gateway-secondary -auto-approve -var-file ${varfile}


echo "Sleep 30 seconds before deploying HelloWorld service"
sleep 30
terraform apply -target null_resource.deploy_hello -auto-approve -var-file ${varfile}
