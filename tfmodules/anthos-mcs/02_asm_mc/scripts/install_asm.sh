#!/bin/zsh
set -e

if [ "$#" -lt 4 ]; then
    >&2 echo "Not all expected arguments set."
    exit 1
fi

PROJECT_ID=$1

CLUSTER_NAME=$2
CLUSTER_LOCATION=$3
ASM_VERSION=$4
OUTPUT_DIR=$5
#MANAGED=$6

MODE="install"

# Download the correct version of the install_asm script
curl https://storage.googleapis.com/csm-artifacts/asm/install_asm_"${ASM_VERSION}" > install_asm
chmod u+x install_asm

declare -a params=(
    "--verbose"
    "--project_id ${PROJECT_ID}"
    "--cluster_name ${CLUSTER_NAME}"
    "--cluster_location ${CLUSTER_LOCATION}"
    "--output_dir ${OUTPUT_DIR}"
    "--mode ${MODE}"
    "--enable-all"
    "--enable-registration"
    "--custom_overlay ../istio-clusterip.yaml"
)

# Run the script with appropriate flags
echo "Running ./install_asm" "${params[@]}"
./install_asm $(echo "${params[@]}")

#if [[ "${MANAGED}" == true ]]; then
#    echo "Running ./install_asm --managed" "${params[@]}"
#    ./install_asm  --managed $(echo "${params[@]}")
#else 
#    echo "Running ./install_asm" "${params[@]}"
#    ./install_asm $(echo "${params[@]}") 
#fi
