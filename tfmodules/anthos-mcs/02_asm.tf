module "asm-primary" {
  source          = "./02_asm_mc"
  cluster_name    = var.primary_cluster
  location        = var.primary_zones[0]
  project_id      = var.project_id
  asm_dir         = "${path.module}/asm-primary"
  cluster_endpoint = "${module.cluster-primary.endpoint}"
  service_account_key_file = var.gcp_key_path
  asm_version     = "1.9"
}

module "asm-secondary" {
  source          = "./02_asm_mc"
  cluster_name    = var.secondary_cluster
  location        = var.secondary_zones[0]
  project_id      = var.project_id
  asm_dir         = "${path.module}/asm-secondary"
  cluster_endpoint = "${module.cluster-secondary.endpoint}"
  service_account_key_file = var.gcp_key_path
  asm_version     = "1.9"
}

module "gateway-primary" {
  source = "terraform-google-modules/gcloud/google//modules/kubectl-wrapper"
  project_id              = var.project_id
  cluster_name            = var.primary_cluster
  cluster_location        = var.primary_zones[0]
  enabled                 = true
  kubectl_create_command  = "kubectl apply -f ${path.module}/istio-manifests/istio-ingressgateway.yaml"
  kubectl_destroy_command = "kubectl delete -f ${path.module}/istio-manifests/istio-ingressgateway.yaml"
  module_depends_on       = [null_resource.trust-establish]
}

module "gateway-secondary" {
  source = "terraform-google-modules/gcloud/google//modules/kubectl-wrapper"
  project_id              = var.project_id
  cluster_name            = var.secondary_cluster
  cluster_location        = var.secondary_zones[0]
  enabled                 = true
  kubectl_create_command  = "kubectl apply -f ${path.module}/istio-manifests/istio-ingressgateway.yaml"
  kubectl_destroy_command = "kubectl delete -f ${path.module}/istio-manifests/istio-ingressgateway.yaml"
  module_depends_on       = [null_resource.trust-establish]
}

locals {
    context1  =  "gke_${var.project_id}_${var.primary_zones[0]}_${var.primary_cluster}"
    context2  =  "gke_${var.project_id}_${var.secondary_zones[0]}_${var.secondary_cluster}"
}
# We wait for all of our microservices to become available on kubernetes
resource "null_resource" "trust-establish" {
  provisioner "local-exec" {
    command = <<-EOT
    echo "apply istioctl to context ${local.context2} with secret from ${var.primary_cluster}"
    istioctl x create-remote-secret --context=${local.context1} --name=${var.primary_cluster} | \
    kubectl apply -f - --context=${local.context2}

    echo "apply istioctl to context ${local.context1} with secret from ${var.secondary_cluster}"    
    istioctl x create-remote-secret --context=${local.context2} --name=${var.secondary_cluster} | \
    kubectl apply -f - --context=${local.context1}    
  EOT
  }
  provisioner "local-exec" {
    when    = destroy
    command = <<-EOT
    echo "run destroy command......................................................"
    kubectl delete secret istio-remote-secret-cluster-17bk   -n istio-system --context="gke_shawn-demo-2021_asia-east1-a_cluster-17"
    kubectl delete secret istio-remote-secret-cluster-17 -n istio-system --context="gke_shawn-demo-2021_asia-northeast1-a_cluster-17bk"
  EOT
  }
  triggers = {
    "before" = module.asm-secondary.asm_wait
  }
}

