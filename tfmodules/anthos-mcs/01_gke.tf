module "cluster-primary" {
  source          = "./01_gke"
  cluster_name    = var.primary_cluster
  location        = var.primary_zones[0]
  project_id      = var.project_id
}
module "cluster-secondary" {
  source          = "./01_gke"
  cluster_name    = var.secondary_cluster
  location        = var.secondary_zones[0]
  project_id      = var.project_id
}