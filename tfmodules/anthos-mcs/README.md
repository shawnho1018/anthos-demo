# Multi-cluster Ingress/Services (MCI/MCS) + Anthos Service Mesh
This project is to demonstrate how to utilize GCP's GCLB and cross cluster service mesh to create a two-tiered service cross two GKE clusters. The configuration scenarios include the following steps and could be easily viewed from install.sh:
## Day 0: Install
The following scripts have been tested on MacOS Big Sur 11.4 with ZShell (iterm2). Additional CLIs are required including
* Terraform (0.15.5)
* gcloud (SDK 343.0.0)
* kpt (0.39.2)
* kubectl  (1.18.18)
* istioctl (1.9.5-asm.2)
* kustomize (4.1.3)
* jq (1.6)

### Step 1: Create GKE and Install ASM
Using terraform to quickly enable GCP services for ASM and Multicluster Ingress/Services. Then, create two clusters and install Anthos service mesh. At last, establish cross-cluster mutual trusts to bring up the service mesh between two clusters. 

```
#/bin/zsh
# Note: when use install.sh, please provide your own .tfvars. You could refer to shawn.tfvars.
varfile=''
if [[ -n "$1" ]]
  then
    varfile=$1
  else
    echo "Use default shawn.tfvars instead"
    varfile=./shawn.tfvars
fi

echo "Enable Project Services"
terraform apply -target module.project-services -auto-approve -var-file ${varfile}

echo "Deploy GKE clusters"
terraform apply -target module.cluster-primary -auto-approve -var-file ${varfile}
terraform apply -target module.cluster-secondary -auto-approve -var-file ${varfile}

echo "Deploy ASM to both clusters"
terraform apply -target module.asm-primary -auto-approve -var-file ${varfile}
terraform apply -target module.asm-secondary -auto-approve -var-file ${varfile}

echo "Configure cross-cluster capability"
terraform apply -target null_resource.trust-establish -auto-approve -var-file ${varfile}
```
The following diagram shows the topology after this step.
![Topology after Step1](./images/tf-1.png)

### Step 2: Install FrontEnd/Backend Services + Mesh Config
This part is to deploy [whereami test services](https://github.com/GoogleCloudPlatform/kubernetes-engine-samples/tree/master/whereami/k8s) to both clusters. We utilize kustomize to create different sets of yaml files for each cluster. 

In the out-of-box (OOB) configuration, there are a frontend and a backend services, created in the built-up process. From istio-ingressgateway to the frontend service, we chose to use [Localized Loadbalancer](https://istio.io/latest/docs/tasks/traffic-management/locality-load-balancing/failover/), provided by Istio. This setup will redirect user's flow to the local pod and failover when the local pod failed from the outlier detection. Between frontend and backend, I apply the subset capability in DestinationRules. By default, frontend pod in Taiwan will call backend pod in Taiwan and vice versa. Those behaviors could be further verified in the latter section.

```
echo "Deploy frontend/backend services"
terraform apply -target null_resource.deploy-services -auto-approve -var-file ${varfile}
```
The following diagram shows the topology after this step.
![Topology after Step2](./images/tf-2.png)

### Step 3: Config CloudEndpoint and MCI/MCS
The final step is to reserve a global (any cast) IP address. Then, MCS requires a config cluster. We choose to use primary cluster as the config cluster and the setup-mcs-config is to enable this feature. Finally, we create the MCS and MCI in config-frontend step. 

MCS is actually a K8s controller, deployed in the config cluster. Its main goal is to produce the corresponding network endpoint group (NEG) for MCI. It uses the corresponding selector to search target pods and secretly derived a service with ClusterIP and its corresponding annotation for NEC. 

```
echo "1. Create LoadBalancer IP, 2. Enable gcloud hub for MCS, 3. Deploy MCS and MCI"
terraform apply -target google_compute_global_address.static -auto-approve -var-file ${varfile}
terraform apply -target null_resource.setup-mcs-config -auto-approve -var-file ${varfile}
terraform apply -target null_resource.config-frontend -auto-approve -var-file ${varfile}
```
The following diagram shows the topology after this step.
![Topology after Step3](./images/tf-4.png)

## Day 1: Observe OOB user flow
Since this lab is set up with GCLB, GCP's GCLB will route client requests to the clusters which has the minimal latency. The following diagram is the best to show the OOB's user dataflow.
![User Data Flow to validate](./images/flow-1.png)

To validate user dataflow in both regions, we setup two clients, one is in Taiwan and the other is in Japan, to validate the out-of-box user flow. 
```
# Validate from my own MacBook in Taiwan
anthos-demo % while true; do curl frontend.endpoints.shawn-demo-2021.cloud.goog; sleep 1; done
{
  "backend_result": {
    "cluster_name": "cluster-tw",
    "host_header": "whereami-backend.backend.svc.cluster.local",
    "metadata": "backend",
    "node_name": "gke-cluster-tw-terraform-202106130200-df47f3ac-cq07.c.shawn-demo-2021.internal",
    "pod_ip": "10.12.1.39",
    "pod_name": "whereami-backend-545c85fd4c-r77qg",
    "pod_name_emoji": "🙃",
    "pod_namespace": "backend",
    "pod_service_account": "whereami-ksa-backend",
    "project_id": "shawn-demo-2021",
    "timestamp": "2021-06-14T17:49:12",
    "zone": "asia-east1-a"
  },
  "cluster_name": "cluster-tw",
  "host_header": "frontend.endpoints.shawn-demo-2021.cloud.goog",
  "metadata": "frontend",
  "node_name": "gke-cluster-tw-terraform-202106130200-df47f3ac-q5jm.c.shawn-demo-2021.internal",
  "pod_ip": "10.12.0.26",
  "pod_name": "whereami-frontend-75f498c657-n5fvm",
  "pod_name_emoji": "👩🏿‍❤️‍👨🏿",
  "pod_namespace": "frontend",
  "pod_service_account": "whereami-ksa-frontend",
  "project_id": "shawn-demo-2021",
  "timestamp": "2021-06-14T17:49:11",
  "zone": "asia-east1-a"
}
```
The other test uses a GCE VM in asia-northeast1-a zone. 
```
# Validatef from GCE VM in Japan zone.
shawnho@jp-tester:~$ while true; do curl frontend.endpoints.shawn-demo-2021.cloud.goog; sleep 1; done
{
  "backend_result": {
    "cluster_name": "cluster-jp",
    "host_header": "whereami-backend.backend.svc.cluster.local",
    "metadata": "backend",
    "node_name": "gke-cluster-jp-terraform-202106130205-e7804b50-mv7d.c.shawn-demo-2021.internal",
    "pod_ip": "10.108.0.26",
    "pod_name": "whereami-backend-5c99cb89db-tj4h5",
    "pod_name_emoji": "🖕🏻",
    "pod_namespace": "backend",
    "pod_service_account": "whereami-ksa-backend",
    "project_id": "shawn-demo-2021",
    "timestamp": "2021-06-14T10:13:49",
    "zone": "asia-northeast1-a"
  },
  "cluster_name": "cluster-jp",
  "host_header": "frontend.endpoints.shawn-demo-2021.cloud.goog",
  "metadata": "frontend",
  "node_name": "gke-cluster-jp-terraform-202106130205-e7804b50-mv7d.c.shawn-demo-2021.internal",
  "pod_ip": "10.108.0.27",
  "pod_name": "whereami-frontend-64644cc474-vmggf",
  "pod_name_emoji": "🌋",
  "pod_namespace": "frontend",
  "pod_service_account": "whereami-ksa-frontend",
  "project_id": "shawn-demo-2021",
  "timestamp": "2021-06-14T10:13:49",
  "zone": "asia-northeast1-a"
}
{
  "backend_result": {
    "cluster_name": "cluster-jp",
    "host_header": "whereami-backend.backend.svc.cluster.local",
    "metadata": "backend",
    "node_name": "gke-cluster-jp-terraform-202106130205-e7804b50-bfxc.c.shawn-demo-2021.internal",
    "pod_ip": "10.108.1.25",
    "pod_name": "whereami-backend-5c99cb89db-ztjwt",
    "pod_name_emoji": "🌆",
    "pod_namespace": "backend",
    "pod_service_account": "whereami-ksa-backend",
    "project_id": "shawn-demo-2021",
    "timestamp": "2021-06-14T10:13:50",
    "zone": "asia-northeast1-a"
  },
```
