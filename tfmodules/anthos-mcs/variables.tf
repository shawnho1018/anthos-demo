variable "project_id" {
  description = "The project ID to host the cluster in"
  default = "shawn-demo-2021"
}
variable "primary_cluster" {
  description = "Name of the cluster"
}
variable "primary_region" {
  description = "The primary region to be used"
  default = "asia-east1"
}
variable "primary_zones" {
  description = "The primary zones to be used"
  default = ["asia-east1-a"]
}
variable "secondary_cluster" {
  description = "Name of the cluster"
}
variable "secondary_region" {
  description = "The secondary region to be used"
  default = "asia-northeast1"
}
variable "secondary_zones" {
  description = "The secondary zone to be used"
  default = ["asia-northeast1-a"]
}

variable "gcp_key_path" {
  description = "Existing GCP service account key path"
  default = "/Users/shawnho/workspace/gcp-keys/shawn-demo-2021-sa.key"
}

variable "asm_revision" {
  description = "ASM revision version, used by ASM auto-injection"
}