#!/bin/zsh
varfile=''
if [[ -n "$1" ]]
  then
    varfile=$1
  else
    echo "Use default shawn.tfvars instead"
    varfile=./shawn.tfvars
fi
function Message(){
    echo "$1"
    echo "Press any key to continue..."
    read -n 1
}

echo "Enable Project Services"
terraform apply -target module.project-services -auto-approve -var-file ${varfile}

echo "Deploy GKE clusters"
terraform apply -target module.cluster-primary -auto-approve -var-file ${varfile}
terraform apply -target module.cluster-secondary -auto-approve -var-file ${varfile}

echo "Deploy ASM to both clusters"
terraform apply -target module.asm-primary -auto-approve -var-file ${varfile}
terraform apply -target module.asm-secondary -auto-approve -var-file ${varfile}

echo "Configure cross-cluster capability"
terraform apply -target null_resource.trust-establish -auto-approve -var-file ${varfile}

echo "Deploy frontend/backend services"
terraform apply -target null_resource.deploy-services -auto-approve -var-file ${varfile}

echo "1. Create LoadBalancer IP, 2. Enable gcloud hub for MCS, 3. Deploy MCS and MCI"
terraform apply -target google_compute_global_address.static -auto-approve -var-file ${varfile}
terraform apply -target null_resource.setup-mcs-config -auto-approve -var-file ${varfile}
terraform apply -target null_resource.config-frontend -auto-approve -var-file ${varfile}
