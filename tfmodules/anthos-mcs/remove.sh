#!/bin/zsh
varfile=''
if [[ -n "$1" ]]
  then
    varfile=$1
  else
    echo "Use default shawn.tfvars instead"
    varfile=./shawn.tfvars
fi
function Message(){
    echo "$1"
    echo "Press any key to continue..."
    read -n 1
}
echo "Destroy Frontend and LoadBalancer IP"
terraform destroy -target null_resource.config-frontend -auto-approve -var-file ${varfile}
terraform destroy -target null_resource.setup-mcs-config -auto-approve -var-file ${varfile}
terraform destroy -target google_compute_global_address.static -auto-approve -var-file ${varfile}

echo "Destroy ASM to both clusters"
terraform destroy -target module.asm-primary -auto-approve -var-file ${varfile}
terraform destroy -target module.asm-secondary -auto-approve -var-file ${varfile}

echo "Destroy GKE clusters"
terraform destroy -target module.cluster-primary -auto-approve -var-file ${varfile}
terraform destroy -target module.cluster-secondary -auto-approve -var-file ${varfile}

