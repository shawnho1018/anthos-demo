resource "google_compute_global_address" "static" {
  name = "mcs-ip"
}
resource "null_resource" "setup-mcs-config" {
    triggers = {
        project_id = var.project_id
        primary_cluster = var.primary_cluster
    }
    provisioner "local-exec" {
        command = "gcloud alpha container hub ingress enable --config-membership=projects/${self.triggers.project_id}/locations/global/memberships/${self.triggers.primary_cluster}"
    }
    provisioner "local-exec" {
        when = destroy
        command = "gcloud alpha container hub ingress disable --config-membership=projects/${self.triggers.project_id}/locations/global/memberships/${self.triggers.primary_cluster}"
    }    
}
resource "null_resource" "config-frontend" {
    triggers = {
        project_id = var.project_id
        gclb_ip = google_compute_global_address.static.address
        primary_cluster = var.primary_cluster
        primary_zone = var.primary_zones[0]
        secondary_cluster = var.secondary_cluster
        secondary_zone = var.secondary_zones[0]
    }
    provisioner "local-exec" {
        command = "03_services/config-frontend.sh ${self.triggers.project_id} ${self.triggers.gclb_ip} ${self.triggers.primary_cluster} ${self.triggers.primary_zone} ${self.triggers.secondary_cluster} ${self.triggers.secondary_zone}"
    }
    provisioner "local-exec" {
        when = destroy
        command = "03_services/delete-frontend.sh ${self.triggers.project_id} ${self.triggers.primary_cluster} ${self.triggers.primary_zone}"
    }
}
resource "null_resource" "deploy-services" {
  triggers = {
      project_id = var.project_id
      asm_revision = var.asm_revision
      primary_cluster = var.primary_cluster
      primary_zone = var.primary_zones[0]
      secondary_cluster = var.secondary_cluster
      secondary_zone = var.secondary_zones[0]
  }
  provisioner "local-exec" {
      command = "03_services/deploy-services.sh ${self.triggers.project_id} ${self.triggers.asm_revision} ${self.triggers.primary_cluster} ${self.triggers.primary_zone} ${self.triggers.secondary_cluster} ${self.triggers.secondary_zone}"
  }
  provisioner "local-exec" {
      when = destroy
      command = "03_services/delete-services.sh ${self.triggers.project_id} ${self.triggers.primary_cluster} ${self.triggers.primary_zone} ${self.triggers.secondary_cluster} ${self.triggers.secondary_zone}"
  }
}
