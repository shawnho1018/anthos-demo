#!/bin/bash

PROJECT=$1
CLUSTER_PRIMARY=$2
ZONE_PRIMARY=$3

CLUSTER_1="gke_${PROJECT}_${ZONE_PRIMARY}_${CLUSTER_PRIMARY}"
kubectl delete -f 03_services/whereami-mci --context ${CLUSTER_1}
