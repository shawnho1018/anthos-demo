#!/bin/zsh
PROJECT=$1
ASM_REVISION=$2
CLUSTER_PRIMARY=$3
ZONE_PRIMARY=$4
CLUSTER_SECONDARY=$5
ZONE_SECONDARY=$6

CLUSTER_1="gke_${PROJECT}_${ZONE_PRIMARY}_${CLUSTER_PRIMARY}"
CLUSTER_2="gke_${PROJECT}_${ZONE_SECONDARY}_${CLUSTER_SECONDARY}"
for c in ${CLUSTER_1} ${CLUSTER_2}; do
    kubectl create ns frontend --context $c
    kubectl label namespace frontend  istio.io/rev=${ASM_REVISION} --context $c
    kubectl create ns backend --context $c
    kubectl label namespace backend  istio.io/rev=${ASM_REVISION}  --context $c
    kubectl apply -k 03_services/whereami-backend/variant --context $c
    if [ $c = ${CLUSTER_1} ]; then
      kubectl apply -k 03_services/whereami-frontend/variant-tw --context $c
      kubectl apply -k 03_services/whereami-mesh/variant-tw --context $c
    elif [ $c = ${CLUSTER_2} ]; then
      kubectl apply -k 03_services/whereami-frontend/variant-jp --context $c
      kubectl apply -k 03_services/whereami-mesh/variant-jp --context $c
    fi
    kubectl apply -f 03_services/whereami-mesh/ --context $c
done

