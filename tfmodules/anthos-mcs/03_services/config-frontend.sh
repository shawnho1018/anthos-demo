#!/bin/zsh
MCI_SVC="whereami-mci"

PROJECT=$1
GCLB_IP=$2
CLUSTER_PRIMARY=$3
ZONE_PRIMARY=$4
CLUSTER_SECONDARY=$5
ZONE_SECONDARY=$6

CLUSTER_1="gke_${PROJECT}_${ZONE_PRIMARY}_${CLUSTER_PRIMARY}"

cat <<EOF > 03_services/dns-spec.yaml
swagger: "2.0"
info:
  description: "Cloud Endpoints DNS"
  title: "Cloud Endpoints DNS"
  version: "1.0.0"
paths: {}
host: "frontend.endpoints.${PROJECT}.cloud.goog"
x-google-endpoints:
- name: "frontend.endpoints.${PROJECT}.cloud.goog"
  target: "${GCLB_IP}"
EOF
gcloud endpoints services deploy 03_services/dns-spec.yaml

# Create MCS
cat <<EOF > 03_services/whereami-mci/mcs.yaml
apiVersion: networking.gke.io/v1beta1
kind: MultiClusterService
metadata:
  name: ${MCI_SVC}
  namespace: istio-system
  annotations:
    beta.cloud.google.com/backend-config: '{"ports": {"80":"gke-ingress-config"}}'
spec:
  template:
    spec:
      selector:
        app: istio-ingressgateway
      ports:
      - name: http
        protocol: TCP
        port: 80
        targetPort: 8080
  clusters:
  - link: "${ZONE_PRIMARY}/${CLUSTER_PRIMARY}"
  - link: "${ZONE_SECONDARY}/${CLUSTER_SECONDARY}"
EOF

# Create MCI
cat <<EOF > 03_services/whereami-mci/mci.yaml
apiVersion: networking.gke.io/v1beta1
kind: MultiClusterIngress
metadata:
  name: istio-ingressgateway-multicluster-ingress
  namespace: istio-system
  annotations:
    networking.gke.io/static-ip: ${GCLB_IP}
spec:
  template:
    spec:
      backend:
       serviceName: ${MCI_SVC}
       servicePort: 80
      rules:
        - host: 'frontend.endpoints.shawn-demo-2021.cloud.goog'
          http:
            paths:
            - path: "/"
              backend:
                serviceName: ${MCI_SVC}
                servicePort: 80
EOF
kubectl apply -f 03_services/whereami-mci/ --context ${CLUSTER_1}
