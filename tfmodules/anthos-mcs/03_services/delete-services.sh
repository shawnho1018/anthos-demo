#!/bin/zsh
PROJECT=$1
CLUSTER_PRIMARY=$2
ZONE_PRIMARY=$3
CLUSTER_SECONDARY=$4
ZONE_SECONDARY=$5

CLUSTER_1="gke_${PROJECT}_${ZONE_PRIMARY}_${CLUSTER_PRIMARY}"
CLUSTER_2="gke_${PROJECT}_${ZONE_SECONDARY}_${CLUSTER_SECONDARY}"
for c in ${CLUSTER_1} ${CLUSTER_2}; do
    if [ $c = ${CLUSTER_1} ]; then
        kubectl delete -k 03_services/whereami-mesh/variant-tw --context $c
    elif [ $c= ${CLUSTER_2} ]; then
        kubectl delete -k 03_services/whereami-mesh/variant-jp --context $c
    fi
    kubectl delete ns frontend --context $c
    kubectl delete ns backend --context $c
done
