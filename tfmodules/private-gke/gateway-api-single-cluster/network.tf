resource "google_compute_network" "cluster-hybrid-network" {
  name = "${var.cluster_name}-network"
  routing_mode            = "GLOBAL"
  auto_create_subnetworks = false
}

resource "google_compute_subnetwork" "cluster-hybrid-subnetwork" {
  name          = "${var.cluster_name}-subnetwork"
  ip_cidr_range = var.subnetwork_range
  network       = google_compute_network.cluster-hybrid-network.id
  private_ip_google_access = true

  secondary_ip_range {
    range_name    = var.subnetwork_pods_range_name
    ip_cidr_range = var.subnetwork_pods
  }

  secondary_ip_range {
    range_name    = var.subnetwork_services_range_name
    ip_cidr_range = var.subnetwork_services
  }

  lifecycle {
    ignore_changes = [secondary_ip_range]
  }
}

resource "google_compute_subnetwork" "proxy-subnetwork" {
  provider        = google-beta

  name            = "${var.cluster_name}-proxy"
  ip_cidr_range   = var.proxy_range
  region          = var.region
  purpose         = "INTERNAL_HTTPS_LOAD_BALANCER"
  role            = "ACTIVE"
  network         = google_compute_network.cluster-hybrid-network.id
}

resource "google_compute_address" "static-ip-runtime" {
  name = "cluster-ingress-loadbalancer"
  address_type = "INTERNAL"
  address = var.ingress_ip

  subnetwork = google_compute_subnetwork.cluster-hybrid-subnetwork.id 
}

resource "google_dns_managed_zone" "googleapis" {
  name     = "googleapis"
  dns_name = "googleapis.com."
  visibility = "private"
  private_visibility_config {
    networks {
      network_url = google_compute_network.cluster-hybrid-network.id
    }
  }
}

resource "google_dns_record_set" "googleapis-a" {
  name         = "private.googleapis.com."
  managed_zone = google_dns_managed_zone.googleapis.name
  type         = "A"
  ttl          = 60

  rrdatas = var.google_private_ips
}

resource "google_dns_record_set" "googleapis-cname" {
  name         = "*.googleapis.com."
  managed_zone = google_dns_managed_zone.googleapis.name
  type         = "CNAME"
  ttl          = 60

  rrdatas = ["private.googleapis.com."]
}

resource "google_dns_managed_zone" "gcr" {
  name     = "gcr"
  dns_name = "gcr.io."
  visibility = "private"
  private_visibility_config {
    networks {
      network_url = google_compute_network.cluster-hybrid-network.id
    }
  }
}

resource "google_dns_record_set" "gcr-a" {
  name         = "gcr.io."
  managed_zone = google_dns_managed_zone.gcr.name
  type         = "A"
  ttl          = 6

  rrdatas = var.google_private_ips
}

resource "google_dns_record_set" "gcr-cname" {
  name         = "*.gcr.io."
  managed_zone = google_dns_managed_zone.gcr.name
  type         = "CNAME"
  ttl          = 60

  rrdatas = ["gcr.io."]
}

resource "google_dns_managed_zone" "pkg" {
  name     = "pkg"
  dns_name = "pkg.dev."
  visibility = "private"
  private_visibility_config {
    networks {
      network_url = google_compute_network.cluster-hybrid-network.id
    }
  }
}

resource "google_dns_record_set" "pkg-a" {
  name         = "pkg.dev."
  managed_zone = google_dns_managed_zone.pkg.name
  type         = "A"
  ttl          = 60

  rrdatas = var.google_private_ips
}

resource "google_dns_record_set" "pkg-cname" {
  name         = "*.pkg.dev."
  managed_zone = google_dns_managed_zone.pkg.name
  type         = "CNAME"
  ttl          = 60

  rrdatas = ["pkg.dev."]
}
