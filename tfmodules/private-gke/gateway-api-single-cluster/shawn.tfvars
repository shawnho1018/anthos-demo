project_id = "shawn-demo-2021"
region = "asia-southeast1"
cluster_name = "pgke-2"
cluster_location = "asia-southeast1-a"
node_locations = null #["europe-west4-a"]
# use an array of zones when working with a regional cluster

subnetwork_range = "10.81.68.128/28"
subnetwork_pods     = "10.63.72.0/21"
proxy_range         = "10.63.80.0/23"
subnetwork_services = "10.63.128.128/25"
master_cidr_block = "10.81.68.112/28"
ingress_ip = "10.81.68.141"

vm_type_runtime = "e2-standard-4"
vm_type_data = "e2-standard-4"

security_group_domain = "google.com"
cluster_service_account_id = "private-gke-install"
subnetwork_pods_range_name = "pod-range"
subnetwork_services_range_name = "svc-range"

