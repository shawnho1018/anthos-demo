#!/bin/zsh
kubectx gke_shawn-demo-2021_asia-east1-a_cluster-eventarc
cd /Users/shawnho/workspace/anthos-demo/tfmodules/private-gke/private-gke-tf/gatewayapi/external-http
# Demo 1: Show All resources
kubectl get gatewayclass
# Highlight YAML
kubectl describe gateway external-http
kubectl describe httproute -n site
kubectl describe httproute 

# Demo 2: Show Routes 
elb="34.149.209.159"
curl -k -H "Host: store.example.com" "https://${elb}/de"
curl -k -H "Host: store.example.com" "https://${elb}"
curl -k -H "Host: site.example.com" "https://${elb}"
curl -k -H "Host: store.example.com" -H "env: canary" "https://${elb}"
