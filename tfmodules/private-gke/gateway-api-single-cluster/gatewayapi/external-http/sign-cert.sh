#!/bin/zsh
fqdn=$1
echo "fqdn=${fqdn}"
openssl genrsa -out "private.key" 2048
cat << EOF >csr-config.cfg
[req]
default_bits              = 2048
req_extensions            = extension_requirements
distinguished_name        = dn_requirements
prompt                    = no

[extension_requirements]
basicConstraints          = CA:FALSE
keyUsage                  = nonRepudiation, digitalSignature, keyEncipherment
subjectAltName            = @sans_list

[dn_requirements]
0.organizationName        = example
commonName                = ${fqdn}

[sans_list]
DNS.1                     = ${fqdn}

EOF
openssl req -new -key ./private.key \
    -out csr.json \
    -config ./csr-config.cfg

openssl x509 -req \
    -signkey private.key \
    -in csr.json \
    -out service-cert \
    -days 730
# Create ssl-certificates from self-signed certificate
gcloud compute ssl-certificates create store-example-com \
    --certificate=service-cert \
    --private-key=private.key \
    --global
# Remove redundant csr file.
rm csr-config.cfg csr.json
