#!/bin/zsh
FQDN="*.example.com"

# Deploy CRD which generates GatewayClass
kubectl kustomize "github.com/kubernetes-sigs/gateway-api/config/crd?ref=v0.3.0" \
| kubectl apply -f -

cd external-http
./sign-cert.sh $FQDN
kubectl apply -f ./
cd ../
