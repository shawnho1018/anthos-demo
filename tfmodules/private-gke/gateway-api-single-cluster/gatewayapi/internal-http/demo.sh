#!/bin/zsh
kubectx gke_shawn-demo-2021_asia-southeast1-a_pgke-2
cd /Users/shawnho/workspace/anthos-demo/tfmodules/private-gke/private-gke-tf/gatewayapi/internal-http
# Demo 1: Show All resources
kubectl get gatewayclass
# Highlight YAML
kubectl describe gateway internal-http
kubectl describe httproute -n site
kubectl describe httproute 

# Demo 2: Show Routes 
gcloud compute ssh jumper --zone asia-southeast1-a
ilb="10.81.68.136"
curl -H "Host: store.example.com" "http://${ilb}/de"
curl -H "Host: site.example.com" "http://${ilb}"
curl -H "Host: store.example.com" -H "env: canary" "http://${ilb}"
# Demo 3: Test for traffic weight
while true; do curl -H "Host: store.example.com" "http://${ilb}"; sleep 1; done