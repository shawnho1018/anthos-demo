# Private GKE Cluster for Testing Gateway API
Please change 
(1) bucket name in config.tf file
(2) the parameters in shawn.tfvars 

to fit your own environment

## Setup Private Cluster
```sh
terraform init
terraform apply -var-file=shawn.tfvars
```

## Deploy Gateway and Workloads
cd gatewayapi/ and run the script, install-internal-gtw.sh

### Explanation of the script
* Deploy GatewayAPI CRD into GKE
```sh
kubectl kustomize "github.com/kubernetes-sigs/gateway-api/config/crd?ref=v0.3.0" \
| kubectl apply -f -
```
* Deploy Gateway
```sh
kubectl apply -f internal-http/gateway-internal.yaml
```
* Deploy Services in different namespaces
```
kubectl apply -f internal-http/store.yaml
kubectl create ns site
kubectl apply -f internal-http/site.yaml
```
* Deploy HTTPRoute
```
kubectl apply -f internal-http/http-route.yaml
kubectl apply -f internal-http/http-route-site.yaml
``
