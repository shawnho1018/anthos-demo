resource "google_compute_firewall" "allow-master-webhook" {
 name    = "gke-master-cluster-webhooks"
 network = google_compute_network.cluster-hybrid-network.name

 direction = "INGRESS"

 allow {
   protocol = "tcp"
   ports    = ["9443"]
 }

 target_tags = ["cluster-runtime"]
 source_ranges = [
     var.master_cidr_block,
     var.subnetwork_range,
     var.subnetwork_pods,
     var.subnetwork_services
     ]
}

resource "google_compute_firewall" "deny-all-egress" {
 name    = "deny-egress"
 network = google_compute_network.cluster-hybrid-network.name

 direction = "EGRESS"
 priority = 9999

 deny {
    protocol = "all"
 }

 destination_ranges = ["0.0.0.0/0"]

}

resource "google_compute_firewall" "allow-gogle-private-egress" {
 name    = "allow-gogle-private-egress"
 network = google_compute_network.cluster-hybrid-network.name

 direction = "EGRESS"
 priority = 1000

 allow {
    protocol = "tcp"
    ports = ["443"]
 }

 destination_ranges = [var.google_private_ip_cidr]

}


resource "google_compute_firewall" "allow-internal-egress" {
 name    = "allow-internal-egress"
 network = google_compute_network.cluster-hybrid-network.name

 direction = "EGRESS"
 priority = 1000

 allow {
    protocol = "all"
 }

 destination_ranges = [
     var.master_cidr_block,
     var.subnetwork_range,
     var.subnetwork_pods,
     var.subnetwork_services
     ]

}

