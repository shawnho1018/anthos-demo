data "google_project" "project" {
}

data "google_service_account" "sa-cluster" {
  account_id   = var.cluster_service_account_id
}


resource "google_container_cluster" "private-gke1" {

  provider = google-beta

  name               = var.cluster_name
  location           = var.cluster_location
  node_locations     = var.node_locations

  release_channel {
    channel = "RAPID"
  }

  # see node pools
  initial_node_count = "1"
  remove_default_node_pool = "true"
  # datapath_provider
  datapath_provider = "ADVANCED_DATAPATH"
  # network
  networking_mode = "VPC_NATIVE"
  network            = google_compute_network.cluster-hybrid-network.name
  subnetwork         = google_compute_subnetwork.cluster-hybrid-subnetwork.name

  master_authorized_networks_config {
    cidr_blocks {
        cidr_block   = "0.0.0.0/0"
        display_name = "allow all IPs to access the master"
    }
  }

  private_cluster_config {
    enable_private_nodes = "true"
    enable_private_endpoint = "false"
    master_ipv4_cidr_block = var.master_cidr_block
  }

  ip_allocation_policy {
    # pod subnet range
    cluster_secondary_range_name  = var.subnetwork_pods_range_name
    # services subnet range
    services_secondary_range_name = var.subnetwork_services_range_name
  }

  resource_labels = {
    mesh_id="proj-${data.google_project.project.number}"
  }

  workload_identity_config {
    identity_namespace = "${data.google_project.project.project_id}.svc.id.goog"
  }

  authenticator_groups_config {
    security_group = "gke-security-groups@${var.security_group_domain}"
  }

  logging_service="logging.googleapis.com/kubernetes"
  monitoring_service="monitoring.googleapis.com/kubernetes"

  addons_config {

    dns_cache_config {
      enabled = true
    }
  }
}

resource "google_container_node_pool" "cluster-runtime" {
  name       = "cluster-runtime"
  location           = var.cluster_location
  node_locations     = var.node_locations

  cluster    = google_container_cluster.private-gke1.name

  initial_node_count = 2

  autoscaling {
    min_node_count = 2
    max_node_count = 4
  }

  node_config {
    machine_type = var.vm_type_runtime
    service_account = data.google_service_account.sa-cluster.email

    metadata = {
      disable-legacy-endpoints = "true"
    }

    oauth_scopes = [
        "https://www.googleapis.com/auth/cloud-platform"
    ]

    tags = [google_container_cluster.private-gke1.name, "cluster-runtime","cluster-${var.project_id}"]
  }

}


