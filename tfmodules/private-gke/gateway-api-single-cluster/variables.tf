variable "project_id" {
  type = string
}

variable "cluster_name" {
  default = "cluster-hybrid"
}

variable "cluster_location" {
  default = "europe-west1-c"
}

variable "region" {
  default = "europe-west1"
}

variable "node_locations" {
  default = [
    "europe-west1-b"
  ]
}

variable "vm_type_runtime" {
  default = "n1-standard-4"
}

variable "vm_type_data" {
  default = "n1-standard-4"
}

variable "master_cidr_block" {
  description = "master cidr range"
}

variable "subnetwork_pods_range_name" {
  description = "pods secondary range name"
  default = "gke-pods-1"
}

variable "subnetwork_services_range_name" {
  description = "services secondary range name"
  default = "gke-services-1"
}

variable "subnetwork_pods" {
  description = "secondary CIDR for pods"
}

variable "subnetwork_services" {
  description = "secondary CIDR for services"
}

variable "ingress_ip" {
  description = "cluster ingress ip"
}

variable "subnetwork_range" {
  description = "nodes cidr range"
}
variable "proxy_range" {
  description = "proxy-only subnet reserved for internal-lb's envoy"
}

# variable "dns_name" {
#   type = string
# }

# cloud nat
variable "nat_ip_allocate_option" {
  # https://cloud.google.com/nat/docs/overview#ip_address_allocation
  description = "AUTO_ONLY or MANUAL_ONLY"
  default     = "AUTO_ONLY"
}

variable "cloud_nat_address_count" {
  # https://cloud.google.com/nat/docs/overview#number_of_nat_ports_and_connections
  description = "the count of external ip address to assign to the cloud-nat object"
  default     = 1
}

variable "security_group_domain" {
  description = "name of the security group"
}

variable "cluster_service_account_id" {
  description = "" # TODO
}

variable "google_private_ips" {
  description = "Private Google API IPs see: https://cloud.google.com/vpc/docs/configure-private-google-access"
  default = ["199.36.153.8", "199.36.153.9", "199.36.153.10", "199.36.153.11"]
}

variable "google_private_ip_cidr" {
  description = "Private Google API IP CIDR see: https://cloud.google.com/vpc/docs/configure-private-google-access"
  default = "199.36.153.8/30"
}
