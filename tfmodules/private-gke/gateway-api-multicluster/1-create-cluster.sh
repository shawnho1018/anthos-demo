#!/bin/zsh
PROJECT_ID=shawn-demo-2021
for c in gke-west-1; do
  gcloud container clusters create $c \
    --zone=us-west1-a \
    --enable-ip-alias \
    --workload-pool=$PROJECT_ID.svc.id.goog \
    --release-channel=rapid \
    --cluster-version=1.20 \
    --project=$PROJECT_ID
  gcloud container clusters get-credentials $c --zone us-west1-a
done
gcloud container clusters create gke-east-1 \
    --zone=us-east1-b \
    --enable-ip-alias \
    --workload-pool=$PROJECT_ID.svc.id.goog \
    --release-channel=rapid \
    --cluster-version=1.20 \
    --project=$PROJECT_ID
gcloud container clusters get-credentials gke-east-1 --zone=us-east1-b --project=$PROJECT_ID
