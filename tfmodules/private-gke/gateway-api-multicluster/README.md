# MultiCluster Gateway API
This project is to demonstrate how to use GKE's multi-cluster ingress service. 

## 0-prerequisite.sh
This script is used to enable googleapis service for multi-cluster service.

## 1-create-cluster.sh
This script is to create two clusters, gke-west-1 and gke-east-1, for MCS's demo.

## 2-register-gke.sh
Register both clusters to Anthos hub. Readers could decide if you want to reduce the cluster number.

## 3-enable-mcs.sh
Enable MCS service & Ingress on HOST_CLUSTER. Once this is applied successfully, we should be able to see all 4 gatewayclass in HOST_CLUSTER. 
```
$ kubectl get gatewayclass

NAME             CONTROLLER
gke-l7-gxlb      networking.gke.io/gateway
gke-l7-gxlb-mc   networking.gke.io/gateway
gke-l7-rilb      networking.gke.io/gateway
gke-l7-rilb-mc   networking.gke.io/gateway
```
[Note] Only HOST_CLUSTER will produce gke-l7-gxlb-mc and gke-l7-rilb-mc gatewayclasses. 

## Apply workloads
Please 
1. Apply mcs-gatewayapi/src-cluster yamls into the cluster without gke-gxlb-mc gatewayclass (in our script, gke-east-1).  
2. kubectl apply -f mcs-gatewayapi/mcs-cluster to the cluster with gke-gxlb-mc (in our script, HOST_CLUSTER=gke-west-1).

After the workload is applied, use the following command to wait gateway being configured correctly (it may take up to 8 minutes). We could retrieve gateway ip address from 
```
$ kubectl get gtw -n store
NAME            CLASS
external-http   gke-l7-gxlb-mc

$ kubectl get gtw external-http -n store -o jsonpath='{.status.addresses[0].value}'; echo
34.149.0.103
```

## Test
1. Create a GCE in us-east1-a to test the MCS connection to our us-east-1 GKE cluster.
2. Update the gip value in mcs-gatewayapi/demo.sh and then run the script. 
