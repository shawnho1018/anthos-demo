#!/bin/zsh
PROJECT_ID=shawn-demo-2021
PROJECT_NUMBER=$(gcloud projects describe ${PROJECT_ID} --format 'value(projectNumber)')
HOST_CLUSTER=cluster-tw
HOST_LOCATION="asia-east1-a"
TARGET_CLUSTER=cluster-jp
TARGET_LOCATION="asia-northeast1-a"

gcloud alpha container hub multi-cluster-services enable \
    --project ${PROJECT_ID}
gcloud projects add-iam-policy-binding $PROJECT_ID \
    --member "serviceAccount:${PROJECT_ID}.svc.id.goog[gke-mcs/gke-mcs-importer]" \
    --role "roles/compute.networkViewer" \
    --project=${PROJECT_ID}

kubectl kustomize "github.com/kubernetes-sigs/gateway-api/config/crd?ref=v0.3.0" \
| kubectl apply --context gke_${PROJECT_ID}_${HOST_LOCATION}_${HOST_CLUSTER} -f -
kubectl kustomize "github.com/kubernetes-sigs/gateway-api/config/crd?ref=v0.3.0" \
| kubectl apply --context gke_${PROJECT_ID}_${TARGET_LOCATION}_${TARGET_CLUSTER} -f -

gcloud alpha container hub ingress enable \
  --config-membership=/projects/${PROJECT_ID}/locations/global/memberships/${HOST_CLUSTER}-${PROJECT_ID}-${HOST_LOCATION} \
  --project=${PROJECT_ID}

gcloud projects add-iam-policy-binding ${PROJECT_ID} \
    --member "serviceAccount:service-${PROJECT_NUMBER}@gcp-sa-multiclusteringress.iam.gserviceaccount.com" \
    --role "roles/container.admin" \
    --project=${PROJECT_ID}
