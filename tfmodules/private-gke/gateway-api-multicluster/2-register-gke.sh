#!/bin/zsh
PROJECT_ID=shawn-demo-2021

gcloud alpha container hub memberships register gke-west-1 \
     --gke-cluster us-west1-a/gke-west-1 \
     --enable-workload-identity \
     --project=$PROJECT_ID

gcloud alpha container hub memberships register gke-east-1 \
     --gke-cluster us-east1-b/gke-east-1 \
     --enable-workload-identity \
     --project=$PROJECT_ID
