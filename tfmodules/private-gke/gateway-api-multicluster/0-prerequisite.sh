#!/bin/zsh
PROJECT_ID=shawn-demo-2021
gcloud services enable gkehub.googleapis.com --project $PROJECT_ID
gcloud services enable dns.googleapis.com --project $PROJECT_ID
gcloud services enable trafficdirector.googleapis.com --project $PROJECT_ID
gcloud services enable cloudresourcemanager.googleapis.com --project $PROJECT_ID

gcloud services enable multiclusterservicediscovery.googleapis.com \
    --project $PROJECT_ID
