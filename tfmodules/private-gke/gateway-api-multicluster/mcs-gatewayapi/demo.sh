#!/bin/zsh
kubectx gke_shawn-demo-2021_asia-east1-a_cluster-tw
cd /Users/shawnho/workspace/anthos-demo/tfmodules/private-gke/private-gke-dual/mcs-exthttp/mcs-cluster
gip="34.149.0.103"
# Demo 1: Show cross cluster traffic
curl -H "Host: tw.store.example.com" "http://${gip}"
curl -H "Host: jp.store.example.com" "http://${gip}"

curl -H "Host: tw.store.example.com" "http://${gip}/east"
curl -H "Host: jp.store.example.com" "http://${gip}/east"

# Demo 2: Show minimal latency
curl -H "Host: store.example.com" "http://${gip}"
gcloud compute ssh jumper --zone us-east1-a
# this script should be run on GCE jumper VM.
curl -H "Host: store.example.com" "http://${gip}"
