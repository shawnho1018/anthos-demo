#!/bin/bash
# The default config_repo will be created under your project 
function create_config_repo() {
  local exist_repo=$(gcloud source repos describe ${CONFIG_REPO_NAME} 2> /dev/null)
  if [ -z "${exist_repo}" ]; then
    echo "Generating ssh-keys for config-repo..."
    if [ -f "${SSH_KEY_PATH}/id_rsa.pub" ]; then
      echo "ssh-key id_rsa.pub exists"
    else 
      echo "Generating ssh-keys"
      ssh-keygen -b 2048 -t rsa -f "${SSH_KEY_PATH}/id_rsa" -q -N ""
      cat "${SSH_KEY_PATH}/id_rsa.pub";echo
      echo "Please add ${SSH_KEY_PATH}/id_rsa.pub to your https://source.cloud.google.com/ page."        
    fi
    waitloop
    gcloud source repos create ${CONFIG_REPO_NAME}
    echo "Create source repo in ${PROJECT_ID}"
    # remove the original remote url (gitlab)
    rm -rf ${CONFIG_REPO_PATH}/.git
    # this part assumes remote repository has been created but has no content yet
    cd ${CONFIG_REPO_PATH}
    git config --global user.name "${ACCOUNT_EMAIL}"
    git config --global user.email "${ACCOUNT_EMAIL}"
    git config --global init.defaultBranch master
    git init
    git add .
    git commit -m "first commit"    
    git config --global credential.https://source.developers.google.com.helper gcloud.sh
    git remote add google https://source.developers.google.com/p/${PROJECT_ID}/r/${CONFIG_REPO_NAME}
    git push -v google -u master
  else
    echo "${CONFIG_REPO_NAME} already exist under the project ${PROJECT_ID}"
  fi    
}
function delete_config_repo() {
    gcloud source repos delete --quiet ${CONFIG_REPO_NAME}
    git remote remove google
  if [ -d ${CONFIG_REPO_PATH} ]; then
    rm -rf ${CONFIG_REPO_PATH}   
  fi 
}
function waitloop() {
    echo "Press 'c' to continue"
    count=0
    while : ; do
        read -n 1 k <&1
        if [[ $k = 'c' ]] ; then
            printf "\nContinue from the script\n"
        break
        else
            ((count=$count+1))
            sleep 2
            printf "\nIterate for $count times\n"
            echo "Press 'c' to continue"
        fi
    done
}
function enable_services() {
    gcloud services enable \
    container.googleapis.com \
    compute.googleapis.com \
    monitoring.googleapis.com \
    logging.googleapis.com \
    meshca.googleapis.com \
    meshconfig.googleapis.com \
    anthos.googleapis.com \
    gkeconnect.googleapis.com \
    gkehub.googleapis.com \
    multiclusteringress.googleapis.com  
}

function create_cluster() {
  local CLUSTER_EXISTS=$(gcloud container clusters describe "${CLUSTER_NAME}" --format="value(name)" 2> /dev/null)
  if [ -z "${CLUSTER_EXISTS}" ]; then
    # Cluster does not exist
    echo "Create clusters at $PROJECT_ID Project"
    echo "Creating a new GKE Cluster...(may take 3-10 minutes)"
    gcloud container clusters create ${CLUSTER_NAME} \
        --release-channel stable \
        --enable-ip-alias \
        --enable-stackdriver-kubernetes \
        --addons=GcePersistentDiskCsiDriver \
        --workload-pool=${PROJECT_ID}.svc.id.goog \
        --enable-autoscaling --min-nodes 1 --max-nodes 4 --num-nodes 3 \
        --machine-type=n1-standard-4 \
        --labels mesh_id="proj-${PROJECT_NUMBER}" \
        --scopes=cloud-platform,logging-write,monitoring-write,pubsub \
        --quiet \
        --zone ${ZONE}
    echo -e "${FANCY_OK} Cluster created, use this command to create your 'kubectl' context configuration:"
  else
    echo -e "${FANCY_NEUTRAL} Cluster already exists, moving on"
  fi
  echo "Re-connect to cluster:  gcloud container clusters get-credentials ${CLUSTER_NAME} --zone ${ZONE} --project ${PROJECT_ID}"
  gcloud container clusters get-credentials ${CLUSTER_NAME} --zone ${ZONE} --project ${PROJECT_ID}
}

function create_autopilot_cluster() {
  local CLUSTER_EXISTS=$(gcloud container clusters describe "${CLUSTER_NAME}" --region=${REGION} --format="value(name)" 2> /dev/null)
  if [ -z "${CLUSTER_EXISTS}" ]; then
    # Cluster does not exist
    echo "Create clusters at $PROJECT_ID Project"
    echo "Creating a new GKE Cluster...(may take 3-10 minutes)"
    echo "Test AutoPilot"
    gcloud container clusters create-auto ${CLUSTER_NAME} \
        --release-channel regular \
        --scopes=cloud-platform,logging-write,monitoring-write,pubsub \
        --quiet \
        --region ${REGION}
    echo -e "${FANCY_OK} Cluster created, use this command to create your 'kubectl' context configuration:"
  else
    echo -e "${FANCY_NEUTRAL} Cluster already exists, moving on"
  fi
  echo "Re-connect to cluster:  gcloud container clusters get-credentials ${CLUSTER_NAME} --region ${REGION} --project ${PROJECT_ID}"
  gcloud container clusters get-credentials ${CLUSTER_NAME} --region ${REGION} --project ${PROJECT_ID}  
}
# This function can be removed later after cilium is GA
function create_cilium_cluster() {
  local CLUSTER_EXISTS=$(gcloud container clusters describe "${CLUSTER_NAME}" --format="value(name)" 2> /dev/null)
  if [ -z "${CLUSTER_EXISTS}" ]; then
    # Cluster does not exist
    echo "Create clusters at $PROJECT_ID Project"
    echo "Creating a new GKE Cluster...(may take 3-10 minutes)"
    echo "Test Cilium as the CNI"
    gcloud beta container clusters create ${CLUSTER_NAME} \
        --release-channel regular \
        --enable-dataplane-v2 \
        --enable-ip-alias \
        --enable-stackdriver-kubernetes \
        --workload-pool=${PROJECT_ID}.svc.id.goog \
        --addons=GcePersistentDiskCsiDriver \
        --enable-autoscaling --min-nodes 1 --max-nodes 4 --num-nodes 3 \
        --machine-type=n1-standard-4 \
        --labels mesh_id="proj-${PROJECT_NUMBER}" \
        --scopes=cloud-platform,logging-write,monitoring-write,pubsub \
        --quiet \
        --zone ${ZONE}
    echo -e "${FANCY_OK} Cluster created, use this command to create your 'kubectl' context configuration:"
  else
    echo -e "${FANCY_NEUTRAL} Cluster already exists, moving on"
  fi
  echo "Re-connect to cluster:  gcloud container clusters get-credentials ${CLUSTER_NAME} --zone ${ZONE} --project ${PROJECT_ID}"
  gcloud container clusters get-credentials ${CLUSTER_NAME} --zone ${ZONE} --project ${PROJECT_ID}
}

function register_cluster() {
  if [ ! -f ${SERVICE_KEY_PATH} ]; then
    gcloud iam service-accounts keys create --iam-account ${SERVICE_ACCOUNT_NAME} ${SERVICE_KEY_PATH}
  else
    echo "service key exists - use the existing key"
  fi  
  if [ ${REGIONAL} = "true" ]; then
    gcloud container hub memberships register ${CLUSTER_NAME} --gke-cluster "${REGION}/${CLUSTER_NAME}" --service-account-key-file="${SERVICE_KEY_PATH}"
  else 
    gcloud container hub memberships register ${CLUSTER_NAME} --gke-cluster "${ZONE}/${CLUSTER_NAME}" --service-account-key-file="${SERVICE_KEY_PATH}"
  fi
}

function unregister_cluster() {
  if [ ${REGIONAL} = "true" ]; then
    gcloud container hub memberships unregister --quiet ${CLUSTER_NAME} --gke-cluster="${REGION}/${CLUSTER_NAME}"
  else 
    gcloud container hub memberships unregister --quiet ${CLUSTER_NAME} --gke-cluster="${ZONE}/${CLUSTER_NAME}"
  fi
}

function install_acm() {
  echo "config-acm"
  if [ -z ${CONFIG_REPO_NAME} ]; then
    echo "Please provide CONFIG_REPO_NAME variable..."
    return 1
  fi  
  gcloud alpha container hub config-management enable
  kubectl create ns config-management-system
  kubectl create secret generic git-creds \
    --namespace=config-management-system \
    --from-file=ssh="${SSH_KEY_PATH}/id_rsa"
cat <<- EOT > ${WORKDIR}/config-management.yaml
apiVersion: configmanagement.gke.io/v1
kind: ConfigManagement
metadata:
  name: config-management
spec:
  git:
    syncRepo: ${CONFIG_REPO}
    syncBranch: master
    secretType: ssh
  policyController:
    enabled: true
EOT
   gcloud alpha container hub config-management apply \
     --membership=${CLUSTER_NAME} \
     --config=${WORKDIR}/config-management.yaml \
     --project=${PROJECT_ID}
}

function delete_acm() {
  echo "delete_acm"
cat <<- EOT > ${WORKDIR}/config-management.yaml
apiVersion: configmanagement.gke.io/v1
kind: ConfigManagement
metadata:
  name: config-management
spec:
  git:
   syncRepo: ${CONFIG_REPO}
   syncBranch: master
   secretType: ssh
  policyController:
    enabled: false
EOT

  gcloud alpha container hub config-management apply \
     --membership=${CLUSTER_NAME} \
     --config=${WORKDIR}/config-management.yaml \
     --project=${PROJECT_ID}
  echo "wait until OPA disabled..."   
  sleep 30
  gcloud alpha container hub config-management delete --quiet \
    --project=${PROJECT_ID} \
    --membership=${CLUSTER_NAME}     
  sleep 5
  gcloud alpha container hub config-management disable \
    --project=${PROJECT_ID}

}

function install_asm() {
  if [[ ! -f install_asm ]]; then
    curl https://storage.googleapis.com/csm-artifacts/asm/install_asm_1.8 > install_asm
    chmod +x install_asm
  fi
  
  if [[ -d "asm_install" ]]; then
     rm -rf asm_install
  fi
  mkdir -p asm_install 
  kubectl create clusterrolebinding istio-admin --clusterrole=cluster-admin --user ${SERVICE_ACCOUNT_NAME}
  LOCATION=${ZONE}
  if [ ${REGIONAL} = "true" ]; then
    LOCATION=${REGION}
  fi
  ./install_asm -v --project_id ${PROJECT_ID} \
    --cluster_name ${CLUSTER_NAME} \
    --cluster_location ${LOCATION} \
    -s ${SERVICE_ACCOUNT_NAME} \
    -k ${SERVICE_KEY_PATH} \
    --mode install \
    --output_dir asm_install \
    --enable_gcp_iam_roles \
    --enable_gcp_apis \
    --enable_gcp_components \
    --enable_cluster_labels
    #asm_install/istioctl install -f asm_install/managed_control_plane_gateway.yaml
}

function delete_asm() {
  #istioctl manifest generate --set profile=asm-gcp | kubectl delete --ignore-not-found=true -f -
  istioctl x uninstall --purge
  kubectl delete ns istio-system
}

function enable_cloudrun() {
  gcloud container clusters update ${CLUSTER_NAME} \
  --update-addons=CloudRun=ENABLED,HttpLoadBalancing=ENABLED \
  --zone=${ZONE}
}

function config_workload_identity() {
  # gsa should be input as name@{project_id}.iam.gserviceaccount.com format"
  if [ ! $# -eq 3 ]; then
    echo "[Error] config_workload_identity: Please specify GSA KSA, and namespace of KSA"
    return 1
  fi 
  local gsa=$1
  local ksa=$2
  local ns=$3
  local gsa_exist=$(gcloud iam service-accounts describe ${gsa} --format="value(name)" 2> /dev/null )
  if [ -z "${gsa_exist}" ]; then
     name=$(echo $gsa | awk '{split($0,array,"@"); print array[1]}')
     echo "Create a GSA: ${name}"
     gcloud iam service-accounts create "${name}"
     gcloud projects add-iam-policy-binding ${PROJECT_ID} \
       --member="${gsa}" \
       --role="roles/owner"     
  fi 

  local ksa_exist=$(kubectl get sa $ksa -n $ns 2> /dev/null )
  if [ -z "${ksa_exist}" ]; then
    echo "Create a KSA $ksa"
    kubectl create sa $ksa -n $ns
    kubectl create clusterrolebinding $ksa -n $ns \
      --serviceaccount $ns:$ksa \
      --clusterrole cluster-admin 
  fi 
  gcloud iam service-accounts add-iam-policy-binding \
    --role roles/iam.workloadIdentityUser \
    --member "serviceAccount:${PROJECT_ID}.svc.id.goog[$ns/$ksa]" \
    "${gsa}"
  kubectl annotate serviceaccount ${ksa} --namespace ${ns} \
    iam.gke.io/gcp-service-account=${gsa}
}

function config_configconnector() {
  kubectl apply -f config-connector/configconnector-operator.yaml
  config_workload_identity ${SERVICE_ACCOUNT_NAME} cnrm-controller-manager cnrm-system
cat << EOF > config-connector.yaml
# config-connector.yaml
apiVersion: core.cnrm.cloud.google.com/v1beta1
kind: ConfigConnector
metadata:
  # the name is restricted to ensure that there is only one
  # ConfigConnector resource installed in your cluster
  name: configconnector.core.cnrm.cloud.google.com
spec:
  mode: cluster
  googleServiceAccount: "${SERVICE_ACCOUNT_NAME}"
EOF
  kubectl apply -f config-connector.yaml
}