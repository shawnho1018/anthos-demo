#!/bin/bash
##############################################
# This is a sample for using gitlab-utility.sh
# Note that RUNNERSECRET must be base64 encode 
# of the token you retrieved from gitlab portal
##############################################
RUNNER_NAME="runner-1"
if [ "$#" > 1 ]; then
  RUNNER_NAME=$1
fi
echo "runner name=$RUNNER_NAME"
source ../gitlab-utility.sh
export WORKDIR=$(pwd)
export RUNNERSECRET="KZFDtS6VCbJS3pcP11RR"
gitlab_ci_initialize
addrunner $RUNNER_NAME
