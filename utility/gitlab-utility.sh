#!/bin/bash
function gitlab_ci_initialize(){
  export PROJECT_ID=$(gcloud config list --format 'value(core.project)')
  if [ -z ${WORKDIR} ]; then
    echo "Please provide WORKDIR variable"
    return 1
  fi 
  if [ -z ${RUNNERSECRET} ]; then
    echo "Please provide RUNNERSECRET variable"
    return 1
  fi
  export PROJECT_ID=$(gcloud config list --format 'value(core.project)')
  export encode_secret=$(echo $RUNNERSECRET | base64)
  if [ ! -f "${WORKDIR}/vars-gitlab.sh" ]; then
    echo "Creating a file with all of the variables for this workshop"
  fi  
cat <<-EOT > ${WORKDIR}/vars-gitlab.sh
export WORKDIR=${WORKDIR}
EOT

cat <<-EOT > ${WORKDIR}/runner-token.yaml
apiVersion: v1
kind: Namespace
metadata:
  name: runners
---
apiVersion: v1
kind: Secret
metadata:
  name: gitlab-runner-secret
  namespace: runners
type: Opaque
data:
  runner-registration-token: ${encode_secret} #base64 encoded registration token
  runner-token: ""
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: gitlab-admin
  namespace: runners
---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  name: gitlab-admin-1
  namespace: runners
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: gitlab-admin
  namespace: runners
EOT
  kubectl apply -f ${WORKDIR}/runner-token.yaml
}
function addrunner() {   
  runner_name=$1
  if [ -z ${runner_name} ]; then
    echo "Need to provide runner name as the input parameter"
    exit 1
  fi 
  helm repo add gitlab https://charts.gitlab.io
  helm install --namespace runners ${runner_name} -f ../gitlab-runner-values.yaml gitlab/gitlab-runner
}
