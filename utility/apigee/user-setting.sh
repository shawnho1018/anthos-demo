#!/bin/bash
export CLOUDDNS_PROJECT="anthos-demo-280104"
export APIGEE_PROJECT="apac-cs-hybrid-demo14"
export APIGEE_REGION="australia-southeast1"
export PROJECT_ID="shawn-demo-2021"
export CLUSTER_NAME="cluster-apigee"
export DOMAIN_NAME="shawnk8s.com"
export CERT_KEY_PATH="${HOME}/workspace/gcp-keys/cert-manager.key"
export CERT_SERVICEACCOUNT="cert-admin@${CLOUDDNS_PROJECT}.iam.gserviceaccount.com"
export CERT_EMAIL="shawnho@google.com"
#############################
# DONT Modify Anything Below
#############################
WORKDIR=$(pwd)

local VERSION=$(curl -s \
    https://storage.googleapis.com/apigee-release/hybrid/apigee-hybrid-setup/current-version.txt)
curl -LO \
    https://storage.googleapis.com/apigee-release/hybrid/apigee-hybrid-setup/$VERSION/apigeectl_mac_64.tar.gz
tar xvzf ./apigeectl_mac_64.tar.gz -C ${WORKDIR}/
mv apigeectl_1.4.2-df6ccff_mac_64 apigeectl
rm apigeectl_mac_64.tar.gz
mkdir -p hybrid-files/overrides
mkdir -p hybrid-files/service-accounts
mkdir -p hybrid-files/certs
export APIGEECTL_HOME=${WORKDIR}/apigeectl
ln -s $APIGEECTL_HOME/tools/ hybrid-files/tools
ln -s $APIGEECTL_HOME/config/ hybrid-files/config
ln -s $APIGEECTL_HOME/templates/ hybrid-files/templates
ln -s $APIGEECTL_HOME/plugins/ hybrid-files/plugins
cat <<EOT > ${WORKDIR}/vars-apigee.sh
export WORKDIR=${WORKDIR}
export PROJECT_ID=${PROJECT_ID}
export CLUSTER_NAME="${CLUSTER_NAME}"
export PROJECT_NUMBER=$(gcloud projects describe ${PROJECT_ID} --format 'value(projectNumber)')
export REGION=$(gcloud config get-value compute/region 2> /dev/null)
export ZONE=$(gcloud config get-value compute/zone 2> /dev/null)
export ACCOUNT_EMAIL=$(gcloud auth list --filter=status:ACTIVE --format="value(account)")
export CLOUDDNS_PROJECT=${CLOUDDNS_PROJECT}
export APIGEE_PROJECT=${APIGEE_PROJECT}
export APIGEE_REGION=${APIGEE_REGION}
export CERT_SERVICEACCOUNT=${CERT_SERVICEACCOUNT}
export CERT_EMAIL=${CERT_EMAIL}
export CERT_KEY_PATH=${CERT_KEY_PATH}
export DOMAIN_NAME=${DOMAIN_NAME}
export APIGEECTL_HOME=${APIGEECTL_HOME}
EOT