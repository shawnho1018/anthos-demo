#!/bin/bash
if [ ! -f "./vars-apigee.sh" ]; then
  echo "Please check if the variable ./vars-apigee.sh exists"
  return 1
fi

function enable_services() {
    gcloud services enable \
    apigee.googleapis.com \
    apigeeconnect.googleapis.com \
    pubsub.googleapis.com \
    cloudresourcemanager.googleapis.com \
    compute.googleapis.com \
    container.googleapis.com --project ${PROJECT_ID} 
}
function install_certmgr() {
    kubectl apply --validate=false -f https://github.com/jetstack/cert-manager/releases/download/v1.2.0/cert-manager.yaml
    kubectl wait -n cert-manager pods -l app=cert-manager --for=condition=ready
    kubectl wait -n cert-manager pods -l app=webhook --for=condition=ready
}
function delete_certmgr() {
    kubectl delete -f https://github.com/jetstack/cert-manager/releases/download/v1.2.0/cert-manager.yaml
}
function delete_cert() {
  kubectl delete -f ${WORKDIR}/cert-issuer.yaml
  kubectl delete secret cert-admin -n istio-system
}
function gen_cert() {
    # CloudDNS may not be in the local project, as the sample here. 
    # If so, we need to retrieve service-account key from the hosted project.
    if [ ! -f "${CERT_KEY_PATH}" ]; then
        echo "generate ${CERT_KEY_PATH}"
        gcloud iam service-accounts keys create ${CERT_KEY_PATH} \
        --iam-account ${CERT_SERVICEACCOUNT}
    else
        echo "service key exists - use the existing key"
    fi
    kubectl create secret generic cert-admin -n istio-system \
    --from-file=${CERT_KEY_PATH}

cat <<- EOT > ${WORKDIR}/cert-issuer.yaml
apiVersion: cert-manager.io/v1
kind: Issuer
metadata:
  name: clouddns-issuer
  namespace: istio-system
spec:
  acme:
    email: ${ACCOUNT_EMAIL}
    server: https://acme-staging-v02.api.letsencrypt.org/directory
    privateKeySecretRef:
      name: clouddns-issuer
    solvers:
    - dns01:
        cloudDNS:
          # The ID of the GCP project
          project: ${CLOUDDNS_PROJECT}
          # This is the secret used to access the service account
          serviceAccountSecretRef:
            name: cert-admin
            key: cert-manager.key
---
apiVersion: cert-manager.io/v1
kind: Certificate
metadata:
  name: cert-default
  namespace: istio-system
spec:
  secretName: cert-default
  issuerRef:
    name: clouddns-issuer
  commonName: "*.${DOMAIN_NAME}"
  dnsNames:
  - "*.${DOMAIN_NAME}"
EOT
    kubectl apply -f cert-issuer.yaml
    kubectl wait certificate/cert-default -n istio-system --for condition=ready --timeout=300s
}

# gen_keys are used to generate/renew apigee keys. It only needs to be executed once a month (key would expire).
# Don't use it during the apigee installation. 

function gen_keys() {
    gcloud config set project ${APIGEE_PROJECT}
    gcloud config set core/account ${}
    gcloud iam service-accounts create apigee-org-admin
    gcloud projects add-iam-policy-binding ${APIGEE_PROJECT} \
    --member serviceAccount:apigee-org-admin@${APIGEE_PROJECT}.iam.gserviceaccount.com \
    --role roles/apigee.admin
    cd hybrid-files
    for id in org-admin synchronizer udca cassandra logger mart metrics watcher; do
        echo "Produce-${id}-key"
        gcloud iam service-accounts keys create "service-accounts/${APIGEE_PROJECT}-apigee-$id.json" \
        --iam-account "apigee-$id@${APIGEE_PROJECT}.iam.gserviceaccount.com"
    done
    cd ../
    gcloud config set project ${PROJECT_ID}
}

function install_apigee() {
    cd hybrid-files
cat <<- EOT > ./overrides/overrides-apigee.yaml
# GCP project name where the org is provisioned.
gcp:
  region: ${APIGEE_REGION}
  projectID: ${APIGEE_PROJECT}

# Apigee org name.
org: ${APIGEE_PROJECT}

# Kubernetes cluster name details
k8sCluster:
  name: ${CLUSTER_NAME}
  region: ${REGION}

# unique identifier for this installation.
instanceID: "${CLUSTER_NAME}-${APIGEE_PROJECT}-2"

virtualhosts:
  - name: apigee-test
    sslSecret: cert-default

envs:
    # Apigee environment name.
  - name: test
    # Service accounts for sync and UDCA.
    serviceAccountPaths:
      synchronizer: ./service-accounts/${APIGEE_PROJECT}-apigee-synchronizer.json
      udca: ./service-accounts/${APIGEE_PROJECT}-apigee-udca.json
mart:
  serviceAccountPath: ./service-accounts/${APIGEE_PROJECT}-apigee-mart.json
metrics:
  serviceAccountPath: ./service-accounts/${APIGEE_PROJECT}-apigee-metrics.json
connectAgent:
  serviceAccountPath: ./service-accounts/${APIGEE_PROJECT}-apigee-mart.json
watcher:
  serviceAccountPath: ./service-accounts/${APIGEE_PROJECT}-apigee-watcher.json
logger:
  enabled: false 
  serviceAccountPath: ./service-accounts/${APIGEE_PROJECT}-apigee-logger.json  
EOT
    # install apigee operator
    echo "##################### apigeectl init #########################"
    apigeectl init -f overrides/overrides-apigee.yaml
    kubectl wait -n apigee-system pods -l control-plane=controller-manager --for condition=ready --timeout 300s 
    kubectl wait -n apigee-system jobs -l job-name=apigee-resources-install --for condition=complete --timeout 300s 
    echo "##################### apigeectl check-ready ##################"
    apigeectl check-ready -f overrides/overrides-apigee.yaml
    echo "##################### apigeectl apply ########################"
    apigeectl apply -f overrides/overrides-apigee.yaml
    kubectl wait pods -l app=apigee-runtime --for condition=ready -n apigee --timeout 600s
    enable_apigee
    cd ${WORKDIR}
}
function enable_apigee() {
    echo "enable apigee config..."
    export GOOGLE_APPLICATION_CREDENTIALS=./service-accounts/${APIGEE_PROJECT}-apigee-org-admin.json
    export TOKEN=$(gcloud auth application-default print-access-token --project ${APIGEE_PROJECT})
    curl -X POST -H "Authorization: Bearer $TOKEN" \
        -H "Content-Type:application/json" \
        "https://apigee.googleapis.com/v1/organizations/${APIGEE_PROJECT}:setSyncAuthorization" \
        -d '{"identities":["'"serviceAccount:apigee-synchronizer@${APIGEE_PROJECT}.iam.gserviceaccount.com"'"]}'
    curl -X POST -H "Authorization: Bearer $TOKEN" \
        -H "Content-Type:application/json" \
        "https://apigee.googleapis.com/v1/organizations/${APIGEE_PROJECT}:getSyncAuthorization" \
        -d ''

    echo "Enable apigee connect..."
    curl -H "Authorization: Bearer $TOKEN" \
        "https://apigee.googleapis.com/v1/organizations/${APIGEE_PROJECT}"
    curl -H "Authorization: Bearer $TOKEN" -X PUT \
        -H "Content-Type: application/json" \
        -d '{
            "name" : "'"${APIGEE_PROJECT}"'",
            "properties" : {
                "property" : [ {
                    "name" : "features.hybrid.enabled",
                    "value" : "true"
            }, {
                "name" : "features.mart.connect.enabled",
                "value" : "true"
                } ]
            }
        }' \
        "https://apigee.googleapis.com/v1/organizations/${APIGEE_PROJECT}"    
}
function delete_apigee() {
    cd hybrid-files
    apigeectl delete -f overrides/overrides-apigee.yaml --all
    cd ../
}