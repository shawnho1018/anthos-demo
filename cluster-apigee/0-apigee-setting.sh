#!/bin/bash
export PROJECT_ID=shawn-demo-2021
export UTILITY_PATH="../utility"
export ACCOUNT_EMAIL="shawnho@google.com"
export SERVICE_KEY_PATH=${HOME}/workspace/gcp-keys/${PROJECT_ID}-sa.key
export SERVICE_ACCOUNT_NAME="anthos-install@${PROJECT_ID}.iam.gserviceaccount.com"
export REGION="asia-east1"
export ZONE="asia-east1-a"
export ITERATION_SUFFIX="apigee"

#######################################################
# DONT MODIFY BELOW THIS LINE
#######################################################
gcloud config set project ${PROJECT_ID}
gcloud config set compute/region ${REGION}
gcloud config set compute/zone ${ZONE}
gcloud config set run/platform gke
gcloud config set run/cluster_location ${ZONE}

if [ ! -f ${SERVICE_KEY_PATH} ]; then
    echo "key has not downloaded yet. Create service key on user's behalf..."
    gcloud iam service-accounts keys create --iam-account ${SERVICE_ACCOUNT_NAME} ${SERVICE_KEY_PATH}
fi

echo "Creating a file with all of the variables for this workshop"
# Repo Name which will be further used for ACM.
# It is recommended NOT ALTER the config-repo name. 
# If the name needs to be altered, please rename the \
# cloned sub-folder (config-repo) to use the same folder name as the name here.
export CONFIG_REPO_NAME="config-repo"
export WORKDIR=$(pwd)
cat <<EOT > ${WORKDIR}/vars-anthos.sh
export WORKDIR=${WORKDIR}
export UTILITY_PATH=${UTILITY_PATH}
export CONFIG_REPO_NAME=${CONFIG_REPO_NAME}
export ITERATION_SUFFIX=${ITERATION_SUFFIX}
export REGIONAL="false"
export CLUSTER_NAME="cluster-${ITERATION_SUFFIX}"
export PROJECT_ID=$(gcloud config list --format 'value(core.project)')
export PROJECT_NUMBER=$(gcloud projects describe ${PROJECT_ID} --format 'value(projectNumber)')
export REGION=$(gcloud config get-value compute/region 2> /dev/null)
export ZONE=$(gcloud config get-value compute/zone 2> /dev/null)
export CONTEXT="gke_${PROJECT_ID}_${ZONE}_${CLUSTER_NAME}"
export ACCOUNT_EMAIL=${ACCOUNT_EMAIL}
export SERVICE_ACCOUNT_NAME=${SERVICE_ACCOUNT_NAME}
# service key location. Key will be automatically generated if it does not present.
export SERVICE_KEY_PATH=${HOME}/workspace/gcp-keys/${PROJECT_ID}-sa.key
export SSH_KEY_PATH=${HOME}/.ssh
# BOTH CONFIG_REPO will be produced if CONFIG_REPO_NAME is provided
export CONFIG_REPO="ssh://${ACCOUNT_EMAIL}@source.developers.google.com:2022/p/${PROJECT_ID}/r/config-repo"
export CONFIG_REPO_PATH="${WORKDIR}/${CONFIG_REPO_NAME}"
source "${UTILITY_PATH}/anthos/anthos-utility.sh"
EOT