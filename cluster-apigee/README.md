# Purpose
This project is to provide a quick poc setup for apigee hybrid. It creates anthos cluster which includes the latest tested components, including:
1. GKE: Release channel standard
2. Anthos registration
3. ASM: 1.8.3
4. ACM: 1.6

Then, we could run the script to further install apigee on top of the Anthos cluster. Please keep in mind that a self-owned domain (registered on GCP) is required for getting a signed certificate from letsencrypt.org. This part of the script would execute the following commands:
1. install cert-manager
2. have your domain (*.[your-domain]) signed. Please make sure you have a service account with DNS administrator role assigned in the project you hosted cloud dns. 
3. config apigee runtime YAML
4. install apigee runtime against apigee control plane. 

# Installation steps:
## Get Apigee Control Plane
 Get a Control Plane from [imedusa](https://imedusa.apigee.net/dashboard). Remeber to grab a "hybrid" one instead of a SaaS apigee. Make sure you could see this project in your console.cloud.google.com as well as apigee.google.com
 
[Note] Since we will not use any gke cluster or certificate inside the control plane, anthos cluster and control plane could be in separate region.  

## Config Apigee Control Plane
Login to apigee.google.com and add a new environment: "test" 
![](images/env.png)
and environment group: "apigee-test"
![](images/env-group.png)

Add the environment into the newly created environment group. Also, add DNS hostname in the environment group.

## Setup Anthos installation variables
Open the 0-anthos-setting.sh and modify variables on the top. Please don't modify anything below the warning line. After modification, please run
```
source 0-anthos-setting.sh
```
It will then create a vars-anthos.sh as the environment variables for installation use. 
## Setup Apigee installation variables
Open the 1-apigee-settings.sh and follow the same step as described in the last step (but with a different source command below)
```
source 1-apigee-setting.sh
```
Once the anthos cluster is ready, check the ip address of istio-ingressgateway under istio-system namespace. Add this ip address and the DNS hostname you specify in step 2 into your CloudDNS.

## Install Anthos Cluster and Apigee components
Then, let's start Anthos cluster creation and configuration by the following command.
```
2-apigee-setup.sh deploy
```
After the installation completes, please further run
```
2-apigee-setup.sh deploy_apigee
```

## Deleting the Anthos/Apigee
If you finish testing the apigee, please run the following commands to remove
```
2-apigee-setup.sh delete_apigee # to delete apigee installation
2-apigee-setup.sh delete # to delete Anthos cluster
```
