#!/bin/bash
ACTION=$1
if [ -z "${ACTION}" ]; then
    echo "an 'ACTION' is required (deploy || delete || deploy_apigee || delete_apigee)"
    exit 1
fi

if [ ${ACTION} = "deploy" ]; then
  echo "deploy..."
  source ./vars-anthos.sh
  enable_services
  create_cluster
  register_cluster
  install_asm
  install_acm
elif [ ${ACTION} = "deploy_apigee" ]; then
  echo "deploy agigee..."
  source ./vars-apigee.sh
  install_certmgr
  sleep 10
  #gen_keys
  gen_cert
  install_apigee
elif [ ${ACTION} = "delete_apigee" ]; then
  echo "delete apigee..."
  source ./vars-apigee.sh
  delete_cert
  delete_certmgr
  delete_apigee  
elif [ ${ACTION} = "delete" ]; then
  echo "delete..."
  source ./vars-anthos.sh
  delete_asm
  delete_acm
  unregister_cluster
  gcloud container clusters delete --quiet $CLUSTER_NAME  
fi