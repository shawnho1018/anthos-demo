#!/bin/bash
export CLOUDDNS_PROJECT="anthos-demo-280104"
export APIGEE_PROJECT="apac-cs-hybrid-demo14"
export APIGEE_REGION="australia-southeast1"
export PROJECT_ID="shawn-demo-2021"
export DOMAIN_NAME="shawnk8s.com"
export CERT_KEY_PATH="${HOME}/workspace/gcp-keys/cert-manager.key"
export CERT_SERVICEACCOUNT="cert-admin@${CLOUDDNS_PROJECT}.iam.gserviceaccount.com"
export ACCOUNT_EMAIL="shawnho@google.com"
export ITERATION_SUFFIX="apigee"
#############################
# DONT Modify Anything Below
#############################
WORKDIR=$(pwd)

local VERSION=$(curl -s \
    https://storage.googleapis.com/apigee-release/hybrid/apigee-hybrid-setup/current-version.txt)
curl -LO \
    https://storage.googleapis.com/apigee-release/hybrid/apigee-hybrid-setup/$VERSION/apigeectl_mac_64.tar.gz
tar xvzf ./apigeectl_mac_64.tar.gz -C ${WORKDIR}/
mv apigeectl_1.4.2-df6ccff_mac_64 apigeectl
rm apigeectl_mac_64.tar.gz
mkdir -p hybrid-files/overrides
mkdir -p hybrid-files/service-accounts
mkdir -p hybrid-files/certs
# Generate keys
source ../utility/apigee/apigee-utility.sh 
cd hybrid-files 
gen_keys
cd ..
export APIGEECTL_HOME=${WORKDIR}/apigeectl
ln -s $APIGEECTL_HOME/tools hybrid-files/
ln -s $APIGEECTL_HOME/config hybrid-files/
ln -s $APIGEECTL_HOME/templates hybrid-files/
ln -s $APIGEECTL_HOME/plugins hybrid-files/
cat <<EOT > ${WORKDIR}/vars-apigee.sh
export WORKDIR=${WORKDIR}
export PROJECT_ID=${PROJECT_ID}
export CLUSTER_NAME="cluster-${ITERATION_SUFFIX}"
export PROJECT_NUMBER=$(gcloud projects describe ${PROJECT_ID} --format 'value(projectNumber)')
export REGION=$(gcloud config get-value compute/region 2> /dev/null)
export ZONE=$(gcloud config get-value compute/zone 2> /dev/null)
export CLOUDDNS_PROJECT=${CLOUDDNS_PROJECT}
export APIGEE_PROJECT=${APIGEE_PROJECT}
export APIGEE_REGION=${APIGEE_REGION}
export CERT_SERVICEACCOUNT=${CERT_SERVICEACCOUNT}
export ACCOUNT_EMAIL=${ACCOUNT_EMAIL}
export CERT_KEY_PATH=${CERT_KEY_PATH}
export DOMAIN_NAME=${DOMAIN_NAME}
export APIGEECTL_HOME=${APIGEECTL_HOME}
source ../utility/apigee/apigee-utility.sh
EOT