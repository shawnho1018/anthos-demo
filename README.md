# Anthos Demonstration
## TL;DR
This project is to show off Anthos's capability in different aspects. The existing demo includes:
* cluster-mesh: Using istio to create cross-cluster traffic between the two clusters in separate subnet.
* cluster-security: Demonstrating how Anthos implements security control. The involved product includes ACM (anthos configuration management) and OPA (open policy agent).
* cluster-serverless: Showing how Anthos could achieve NO-OP day-2 operations with Cloudrun. Once the workload has been deployed, those day-2 headaches, including (1) blue-green deployment and (2) workload autoscaling (based on user session concurrency) are automatically configured. We also demonstrate Cloudrun could be used against Machine-Learning inference services. 
* utility: Core component of the Anthos-related product deployment. 
    * anthos-utility is in charge of anthos cluster creation, registration, ACM installation/configuration, and ASM installation/configuration.
    * gitlab-utility demonstrated how to use existing GKE to serve as Gitlab CI runners. 